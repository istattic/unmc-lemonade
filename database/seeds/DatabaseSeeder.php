<?php

use App\Character;
use App\User;
use App\Highscores;
use App\Texts;
use App\Seeder\BaseSeeder;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserTableSeeder');
        $this->call('CharacterSeeder');
        $this->call('QuestionTableSeeder');
        $this->call('HighscoresTableSeeder');
        $this->call('TextsTableSeeder');
    }

}

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'username' => 'Zac Fowler',
            'email' => 'zfowler@unomaha.edu',
            'password' => bcrypt('pass1234'),
            'role' => 's',
        ]);
    }
}

class HighscoresTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('highscores')->delete();
        $scores = [
            [
                'username' => 'Cassi',
                'score' => '1000',
                'possible' => '1500',
                'character' => '/img/characters/doctors/doctor-f-03-head.png'
            ],
            [
                'username' => 'John',
                'score' => '800',
                'possible' => '1200',
                'character' => '/img/characters/doctors/doctor-m-01-head.png'
            ],
            [
                'username' => 'Joe',
                'score' => '600',
                'possible' => '1000',
                'character' => '/img/characters/doctors/doctor-m-04-head.png'
            ],
            [
                'username' => 'Jane',
                'score' => '400',
                'possible' => '700',
                'character' => '/img/characters/doctors/doctor-f-05-head.png'
            ],
            [
                'username' => 'Jeff',
                'score' => '200',
                'possible' => '600',
                'character' => '/img/characters/doctors/doctor-m-02-head.png'
            ],
        ];
//        Highscores::create($scores);
        DB::table('highscores')->insert($scores);
    }
}

class TextsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('texts')->delete();
        $texts = [
            [
                'section_title' => 'About Project',
                'section_text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare malesuada leo, non vulputate lorem cursus et. Integer ut arcu placerat felis sollicitudin placerat. Donec iaculis posuere quam quis molestie. Etiam tincidunt sit amet tortor et aliquam. Praesent at lorem in justo euismod auctor. Integer tempus, lectus ac mollis ultricies, dolor urna iaculis mauris, sed vulputate diam elit a enim. Nam vitae ex elementum, fringilla erat eget, scelerisque massa. Duis et hendrerit sapien. Nam felis libero, posuere sit amet varius commodo, blandit nec nibh. Integer tristique velit vel lectus condimentum pulvinar. Ut vitae nunc nec nisl lacinia mattis in id ipsum. Nullam turpis metus, eleifend a tempor vitae, dapibus et arcu. Aliquam vitae maximus nulla. Praesent luctus suscipit mi nec luctus. Nulla efficitur purus erat, vel tincidunt metus dapibus quis. Mauris eu elit lacus.

Suspendisse fringilla sit amet felis quis consectetur. Integer sed congue nunc, in tempor ante. Maecenas in metus nisi. Vestibulum et tortor fringilla, consectetur tellus quis, aliquam lorem. Ut ac vehicula purus. Quisque auctor tempor enim, eget maximus lectus imperdiet vehicula. Phasellus purus quam, convallis ac auctor a, tempor sit amet justo. Quisque vitae ornare leo. Maecenas sed pharetra lectus. Quisque ac lorem lectus. Donec non ullamcorper nisl, id elementum diam. Sed et feugiat libero, at tincidunt tellus. Phasellus sit amet sapien mauris. Sed sollicitudin augue sed arcu pulvinar, id tempus lorem egestas. Integer venenatis leo mauris, vitae pulvinar diam egestas vitae. Pellentesque id odio ut libero hendrerit varius.',
            ],
            [
                'section_title' => 'Resources',
                'section_text' => 'Carretie, L., Hinojosa, J. A., Martin-Loeches, M., Mecado, F., & Tapia, M. (2004).',
            ],
        ];
        DB::table('texts')->insert($texts);
    }
}

class CharacterSeeder extends Seeder
{
    public function run()
    {
        DB::table('characters')->delete();
        $characters = [
            [
                'type' => 'doctor',
                'gender' => 'm',
                'number' => '01',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'f',
                'number' => '05',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'f',
                'number' => '03',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'f',
                'number' => '07',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'm',
                'number' => '04',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'm',
                'number' => '06',
                'active' => true
            ],
            [
                'type' => 'doctor',
                'gender' => 'm',
                'number' => '02',
                'active' => true
            ]
        ];
        DB::table('characters')->insert($characters);

        $patients = [
            [
                'type' => 'patient',
                'gender' => 'm',
                'number' => '01',
                'active' => true
            ],
            [
                'type' => 'patient',
                'gender' => 'f',
                'number' => '05',
                'active' => true
            ],
            [
                'type' => 'patient',
                'gender' => 'm',
                'number' => '03',
                'active' => true
            ],
            [
                'type' => 'patient',
                'gender' => 'm',
                'number' => '06',
                'active' => true
            ]
        ];
        DB::table('characters')->insert($patients);
    }
}

class QuestionTableSeeder extends BaseSeeder
{
    //- See more at: http://laravelsnippets.com/snippets/seeding-database-with-csv-files-cleanly#sthash.NyBjNUJ4.dpuf
//  http://laravelsnippets.com/snippets/seeding-database-with-csv-files-cleanly
    public function __construct()
    {
        $this->table = 'questions'; // Your database table name
        $this->filename = app_path() . '/../database/csv/questions.csv'; // Filename and location of data in csv file
    }


}

