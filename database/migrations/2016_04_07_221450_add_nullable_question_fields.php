<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableQuestionFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function ($table) {
            $table->string('picture')->nullable()->change();
            $table->boolean('MD')->default(false)->change();
            $table->boolean('PT')->default(false)->change();
            $table->boolean('PA')->default(false)->change();
            $table->boolean('RX')->default(false)->change();
            $table->boolean('D')->default(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function ($table) {
            $table->string('picture')->change();
            $table->boolean('MD')->change();
            $table->boolean('PT')->change();
            $table->boolean('PA')->change();
            $table->boolean('RX')->change();
            $table->boolean('D')->change();
        });
    }
}
