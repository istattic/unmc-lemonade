<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighscoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('highscores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
            $table->string('username');
            $table->integer('score');
            $table->integer('possible');
            $table->string('character');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('highscores');
	}

}
