<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function(Blueprint $table)
        {
            $table->boolean('MD');
            $table->boolean('PT');
            $table->boolean('PA');
            $table->boolean('RX');
            $table->boolean('D');
            $table->dropColumn('disciplines');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function(Blueprint $table)
        {
            $table->dropColumn('MD');
            $table->dropColumn('PT');
            $table->dropColumn('PA');
            $table->dropColumn('RX');
            $table->dropColumn('D');
            $table->string('disciplines');

        });
    }
}
