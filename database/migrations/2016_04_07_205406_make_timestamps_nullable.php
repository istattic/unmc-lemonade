<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTimestampsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('users', function (Blueprint $table) {
            $table->nullableTimestamps();
        });

        Schema::table('characters', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('characters', function (Blueprint $table) {
            $table->nullableTimestamps();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('questions', function (Blueprint $table) {
            $table->nullableTimestamps();
        });

        Schema::table('highscores', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('highscores', function (Blueprint $table) {
            $table->nullableTimestamps();
        });

        Schema::table('texts', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('texts', function (Blueprint $table) {
            $table->nullableTimestamps();
        });

        Schema::table('logins', function (Blueprint $table) {
            $table->dropTimestamps();
       });
        Schema::table('logins', function (Blueprint $table) {
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('characters', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('characters', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('questions', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('highscores', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('highscores', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('texts', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('texts', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('logins', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('logins', function (Blueprint $table) {
            $table->timestamps();
        });
    }
}
