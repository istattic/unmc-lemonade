<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $fillable = [
        'question',
        'category',
        'value',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'correct_answer',
        'explanation',
        'learning_point',
        'picture',
        'MD',
        'PT',
        'PA',
        'RX',
        'D'
    ];

}
