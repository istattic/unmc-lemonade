<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model {
    // May want to add the doctor code in there too.

    protected $fillable = [
        'type',
        'gender',
        'number',
        'active'
    ];

    public function scopeOfType($query, $type)
    {
        return $query->whereType($type);
    }

    public function scopeDoctor($query)
    {
        $query->whereType('doctor');
    }

    public function scopePatient($query)
    {
        $query->whereType('patient');
    }

    public function getImgPath($v = null)
    {
        $v = $v ? '-' . $v : '';
        $path = sprintf("/img/characters/%ss/%s-%s-%02d$v.png",
                $this->type,
                $this->type,
                $this->gender,
                $this->number
            );
        return $path;
    }

    public function getImgHeadPath() {
        return $this->getImgPath('head');
    }
    public function getImgAltPath() {
        return $this->getImgHeadPath('alt');
    }
}
