<?php namespace App;

use Illuminate\Support\Facades\Session;

class Game
{
    // May want to add the doctor code in there too.

    const session_key = 'game_state';

//    private $session;

    protected $player_name;
    protected $player_character;  // Refactor: make this a Character object?
    protected $earned_score;
    protected $possible_score;
    protected $discipline;

    public $questionsSeen = [];
    public $questions = []; // array of patient Qs, and As from user
    public $shifts = [];
    // also include patient id, response id, etc.

//    If it makes sense to inject session (due to middleware, then do so)
//    public function __construct(Session $session) {
//        $this->session = $session;
//    }

    /**
     * @return int
     */
    public function getEarnedScore()
    {
        return $this->earned_score ? $this->earned_score : 0;
    }


    /**
     * @param int $earned_score
     */
    public function setEarnedScore($earned_score)
    {
        $this->earned_score = $earned_score;
    }


    /**
     * @return mixed
     */
    public function getPlayerCharacter()
    {
        return $this->player_character;
    }

    /**
     * @param mixed $player_character
     */
    public function setPlayerCharacter($player_character)
    {
        $this->player_character = $player_character;
    }

    /**
     * @return string
     */
    public function getPlayerName()
    {
        return $this->player_name;
    }

    /**
     * @param string $player_name
     */
    public function setPlayerName($player_name)
    {
        $this->player_name = $player_name;
    }

    /**
     * @return int
     */
    public function getPossibleScore()
    {
        return $this->possible_score ? $this->possible_score : 0;
    }

    /**
     * @param int $possible_score
     */
    public function setPossibleScore($possible_score)
    {
        $this->possible_score = $possible_score;
    }

    public function getDiscipline()
    {
        return $this->discipline;
    }

    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;
    }

    // Will probably need to store off things like ... characters seen.


    /**
     * Reset scores (possible and earned) to zero
     */
    public function resetScore()
    {
        $this->possible_score = 0;
        $this->earned_score = 0;
    }

    /**
     *  Reset the game state by clearning out the player info and score
     */
    public function resetGame()
    {
        $this->player_character = null;
        $this->player_name = null;
        $this->resetScore();
    }

    /**
     * If the player name is set, the game has been started
     * @return bool
     */
    public function isGameStarted()
    {
        return $this->player_name != "";
    }

//    public function serialize() {
//
//        return serialize($this);
//    }
//    public function unserialize($data) {
//        return unserialize($data);
//    }

    public function addQuestionSeen($id) {
        $this->questionsSeen[] = $id;
    }

    public function getQuestionsSeen() {
        return $this->questionsSeen;
    }

    public function getAllAnsweredQuestions()
    {
        $noOfShifts = count($this->shifts);

        $allQuestions = [];

        for ($i = 0; $i < $noOfShifts; $i++) {
            $allQuestions[$i]['category'] = $this->shifts[$i]['category'];
            $allQuestions[$i]['questions'] = $this->shifts[$i]['questions'];
        }

        return $allQuestions;

    }

    /**
     * Return the current shift's questions answered by a user.
     */
    public function getCurrentShiftQuestions()
    {
        $shiftNo = count($this->shifts) - 1;
        return $this->shifts[$shiftNo]['questions'];
    }

    public function getAllShiftQuestions()
    {

        $shiftNo = count($this->shifts) - 1;

        $shiftQuestions = [];

        $shiftQuestions['category'] = $this->shifts[$shiftNo]['category'];
        $shiftQuestions['questions'] = $this->shifts[$shiftNo]['questions'];


        return $shiftQuestions;

    }

    /*public function getLastShiftQuestion() {
        $shiftNo = count($this->shifts) - 1;
        return $this->shifts[$shiftNo]['questions'];
    }*/

    public function setShift($category)
    {
        $shiftNo = count($this->shifts);
        $this->shifts[$shiftNo]['category'] = $category;
        $this->shifts[$shiftNo]['questions'] = [];
    }

    public function getCurrentShift()
    {
        return last($this->shifts);
    }

    /**
     * Add a new question to the end of the list
     *   Array has 'question' -> text of question and 'patient' -> ID of patient character
     *
     * @param array $questionToAdd
     */
    public function addQuestion($questionToAdd)
    {
        $shiftNo = count($this->shifts) - 1;
        $this->shifts[$shiftNo]['questions'][] = $questionToAdd;
//        $this->questions[] = $questionToAdd;
    }


    /**
     * Remove the last question for the current shift
     * Used for handling refreshing on stand page,
     * if player wants a different question
     */
    public function unsetLastQuestion()
    {
        $shiftNo = count($this->shifts) - 1;
        $questionCount = count($this->shifts[$shiftNo]['questions']);
        unset($this->shifts[$shiftNo]['questions'][$questionCount - 1]);
    }

    public function setQuestionAnswered($question_index, $answer_value)
    {
        $shiftNo = count($this->shifts) - 1;
        $this->shifts[$shiftNo]['questions'][$question_index]['answered'] = $answer_value;
//        $this->questions[$question_index]['answered'] = $answer_value;
    }


    public function addPoints($score)
    {
        $this->earned_score += $score;
    }

    public function increasePossibleScore($score)
    {
        $this->possible_score += $score;
    }

    public function saveState()
    {
        // unsure if this is allowed....
        // no dependency injection is "bad"
        // ???????????
        Session::put(self::session_key, serialize($this));
    }

    public static function loadState()
    {
        if (Session::has(self::session_key)) {
            return unserialize(Session::get(self::session_key));
        } else {
            die("Something went horribly wrong.");
        }
    }
}
