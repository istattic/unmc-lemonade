<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    //set the primary key to be the IP address instead of an ID
    protected $primaryKey = 'ip';

    protected $fillable = [
        'ip',
        'agent',
        'number_login_attempts',
        'first_login_attempt',
        'last_login_attempt'
    ];
}
