<?php
// BaseSeeder.php - this is the file that all seed files should extend.

namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BaseSeeder extends Seeder
{

    /**
     * DB table name
     *
     * @var string
     */
    protected $table;

    /**
     * CSV filename
     *
     * @var string
     */
    protected $filename;

    /**
     * Run DB seed
     */
    public function run()
    {
        DB::table($this->table)->delete();
        $seedData = $this->seedFromCSV($this->filename, ',');

        for($i=0; $i<sizeof($seedData); $i++)
        {
            if(strtoupper($seedData[$i]['MD']) == 'N')
            {
                $seedData[$i]['MD'] = 0;
            }
            elseif(strtoupper($seedData[$i]['MD']) == 'Y')
            {
                $seedData[$i]['MD'] = 1;
            }

            if(strtoupper($seedData[$i]['PA']) == 'N')
            {
                $seedData[$i]['PA'] = 0;
            }
            elseif(strtoupper($seedData[$i]['PA']) == 'Y')
            {
                $seedData[$i]['PA'] = 1;
            }

            if(strtoupper($seedData[$i]['PT']) == 'N')
            {
                $seedData[$i]['PT'] = 0;
            }
            elseif(strtoupper($seedData[$i]['PT']) == 'Y')
            {
                $seedData[$i]['PT'] = 1;
            }

            if(strtoupper($seedData[$i]['RX']) == 'N')
            {
                $seedData[$i]['RX'] = 0;
            }
            elseif(strtoupper($seedData[$i]['RX']) == 'Y')
            {
                $seedData[$i]['RX'] = 1;
            }

            if(strtoupper($seedData[$i]['D']) == 'N')
            {
                $seedData[$i]['D'] = 0;
            }
            elseif(strtoupper($seedData[$i]['D']) == 'Y')
            {
                $seedData[$i]['D'] = 1;
            }
        }

        DB::table($this->table)->insert($seedData);
    }

    /**
     * Collect data from a given CSV file and return as array
     *
     * @param $filename
     * @param string $deliminator
     * @return array|bool
     */
    private function seedFromCSV($filename, $deliminator = ",")
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 4096, $deliminator)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }

            }
            fclose($handle);
        }

        return $data;
    }
}
