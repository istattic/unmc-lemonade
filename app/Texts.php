<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Texts extends Model {

    protected $fillable = ['section_text'];
    protected $table = 'texts';

}
