<?php namespace App\Providers;

use App\Character;
use App\Game;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Getting the game from the session .... HOW?!
        view()->composer('partials.player_badge', function ($view) {
            $g = Game::loadState(); // Refactor: move to global?!

            $current_user = $g->getPlayerName();
            $view->with('current_user', $current_user);

            $discipline = $g->getDiscipline();

            switch ($discipline) {
                case 'MD':
                    $discipline = 'Medical Doctor';
                    break;
                case 'PT':
                    $discipline = 'Physical Therapist';
                    break;
                case 'PA':
                    $discipline = 'Physician\'s Assistant';
                    break;
                case 'RX':
                    $discipline = 'Pharmacist';
                    break;
                case 'D':
                    $discipline = 'Dentist';
                    break;
                default:
                    $discipline;
                    break;
            }


            $view->with('discipline', $discipline);

            $player_character_id = $g->getPlayerCharacter();
            $doctor = Character::find($player_character_id);
            $view->with('player_character_img_url', $doctor->getImgHeadPath());
        });
        view()->composer('partials.player_score', function ($view) {
            $g = Game::loadState(); // Refactor: move to global

            $possible_score = $g->getPossibleScore();
            $view->with('possible_score', $possible_score);

            $earned_score = $g->getEarnedScore();
            $view->with('earned_score', $earned_score);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
