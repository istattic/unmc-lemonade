<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuestionRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'question' => 'required',
            'category' => 'required',
            'value' => 'required',
            'answer_a' => 'required',
            'answer_b' => 'required',
            'answer_c' => 'required',
            'answer_d' => 'required',
            'correct_answer' => 'required',
            'explanation' => 'required',
            'learning_point' => 'required',
            'image' => 'mimes:jpg,png,jpeg,tiff,gif',

        ];
    }

}
