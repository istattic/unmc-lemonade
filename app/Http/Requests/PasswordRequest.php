<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class PasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        if(Auth::user()->role == 'a') {
            return [
                'old_password' =>'required',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required'

            ];
        } else {
            return [
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required'
            ];
        }

	}

}
