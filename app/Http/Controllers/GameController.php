<?php namespace App\Http\Controllers;




use App\Character;
use App\Game;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class GameController extends Controller
{

    /**
     * Start game main menu screen
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $about = DB::table('texts')->where('section_title', 'About Project')->first();

        return view('game.mainmenu', compact('about'));
    }

    /**
     * Show select character screen for user
     */
    public function characters()
    {
        // Already set? Go ahead back into the game.
        if (Session::has('game_state')) {
            $g = Game::loadState();
            if(! $g->shifts) {
                return redirect('stand/selectShift');
            } else {
                return redirect('stand');
            }
        } else {
            $chars = Character::ofType('doctor')->get();
//        $chars = $chars->random(1,4);
            $chars[0]['discipline'] = 'MD';
            $chars[1]['discipline'] = 'PA';
            $chars[2]['discipline'] = 'PT';
            $chars[3]['discipline'] = 'RX';
            return view('game.characters', compact('chars'));
        }
    }

    public function characterSelect(Request $request)
    {
        // Set up initial game info.
        $g = new Game();
        $g->setPlayerName($request->input('player_name'));
        $g->setPlayerCharacter($request->input('doctor'));
        $g->setDiscipline($request->input('discipline'));
        $g->resetScore();

        $g->saveState();

        // Set current user's game character and name.
        flash()->success('Character selected');
        // redirect back to stand.
        return redirect('stand/selectShift');
    }

    public function highScores()
    {
        $scores = DB::table('highscores')->orderBy('score', 'desc')->take(20)->get();

        return view('game.highScores', compact('scores'));
    }

    /**
     * This is the final "shift summary" the player will see when they choose to end the game
     */
    public function finalReview()
    {
        $g = Game::loadState();

        $user = $g->getPlayerName();
        $score = $g->getEarnedScore();
        $possible = $g->getPossibleScore();
        $character = $g->getPlayerCharacter();
        $doctor = Character::find($character);
        $characterPath = $doctor->getImgHeadPath();

        //create a new array to hold all the questions
        $allQuestions = [];
        $correct = 0;
        $totalQuestions = 0;

        $i = 0;
        $s = 0;

        foreach($g->getAllAnsweredQuestions() as $shift) {
            if(!isset($allQuestions[$s]['earned'])) {
                $allQuestions[$s]['earned'] = 0;
                $allQuestions[$s]['correct'] = 0;
            }
            if(!isset($allQuestions[$s]['potential'])) {
                $allQuestions[$s]['potential'] = 0;
                $allQuestions[$s]['total'] = 0;
            }
            $allQuestions[$s]['category'] = $shift['category'];
            foreach($shift['questions'] as $q) {
                if(array_key_exists('answered', $q)) {
                    $allQuestions[$s]['questions'][$i]['question'] = json_decode($q['question']);
                    $allQuestions[$s]['questions'][$i]['answered'] = $q['answered'];
                    $allQuestions[$s]['questions'][$i]['patient'] = $q['patient'];
                    if(strtoupper($allQuestions[$s]['questions'][$i]['answered']) == strtoupper($allQuestions[$s]['questions'][$i]['question']->correct_answer)) {
                        $correct++;
                        $allQuestions[$s]['earned'] = $allQuestions[$s]['earned'] + $allQuestions[$s]['questions'][$i]['question']->value;
                        $allQuestions[$s]['correct']++;
                    }
                    $allQuestions[$s]['potential'] = $allQuestions[$s]['potential'] + $allQuestions[$s]['questions'][$i]['question']->value;
                    $allQuestions[$s]['total']++;
                    $totalQuestions++;
                }
                $i++;
            }
            $s++;
        }

        if ($totalQuestions == 0) {

            $g->resetGame();
            Session::flush();

            flash('Thanks for playing!')->important();
            return redirect('/');
        }

        DB::table('highscores')->insert(
            ['username' => $user, 'score' => $score, 'possible' => $possible, 'character' => $characterPath]
        );

        flash('Thanks for playing!')->important();

        return view('game.finalReview', compact('user', 'score', 'possible', 'allQuestions', 'correct', 'totalQuestions', 'shiftScores'));
    }

    public function endGame()
    {
        $g = Game::loadState();

        $g->resetGame();
        Session::flush();

        return redirect('/');
    }

    public function fullQuestionList()
    {

        if(!Game::loadState()) {
            flash('You do not have permission to see the full question list.  Play the game and earn a score of $10,000 or higher to see all questions!');
            return redirect('/');
        }

        $g = Game::loadState();

        $score = $g->getEarnedScore();

        if (!$score || $score < 10000) {
            flash('You do not have permission to see the full question list.  Play the game and earn a score of $10,000 or higher to see all questions!');
            return redirect('/');
        }

        //get all the category options for the questions
        $options = $this->getOptionsArray();
        $allCategoriesWithQuestions = [];

        for($i = 0; $i < count($options); $i++) {
            $allCategoriesWithQuestions[$i]['category'] = strtolower($options[$i]->category);
            $allCategoriesWithQuestions[$i]['questions'] = DB::table('questions')->where('category', $options[$i]->category)->get();
        }

        return view('game.fullQuestionList',
            compact(
                'options',
                'allCategoriesWithQuestions'
            )
        );
    }

    public function about()
    {
        $about = DB::table('texts')->where('section_title', 'About Project')->first();
        $resources = DB::table('texts')->where('section_title', 'Resources')->first();

        return view('about', compact('about', 'resources'));
    }

    private function getOptionsArray()
    {
        $options = DB::table('questions')->select('category')->groupBy('category')->get();

        return $options;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function gameInstructions()
    {
        return view('game.instructions');
    }


}
