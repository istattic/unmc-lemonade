<?php namespace App\Http\Controllers;

use App\Http\Requests\CSVRequest;
use App\Http\Requests\QuestionRequest;
use App\Question;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;
use Intervention\Image\Facades\Image;

class QuestionsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Questions Controller
    |--------------------------------------------------------------------------
    |
    | Manages the admin user's back-end CRUD system for questions
    | Adding an extra line of code for testing openshift!
    |
    */

    public function index()
    {

        $options = $this->getOptionsArray();
        $allCategoriesWithQuestions = [];

        for($i = 0; $i < count($options); $i++) {
            $allCategoriesWithQuestions[$i]['category'] = strtolower($options[$i]->category);
            $allCategoriesWithQuestions[$i]['questions'] = DB::table('questions')->where('category', $options[$i]->category)->get();
        }

        return view('questions.index',
            compact(
                'options',
                'allCategoriesWithQuestions'
            )
        );
    }

    public function show($id)
    {
        //find the question based on the id
        $question = Question::find($id);
        return view('questions.show', compact('question'));
    }

    public function create()
    {
        //get all options for question categories for returning to the view and populating the dropdown
        $optionsObject = $this->getOptionsArray();
        $options = [];
        for($i=0;$i<count($optionsObject);$i++) {
            $options[] = $optionsObject[$i]->category;
        }
        return view ('questions.create', compact('options'));
    }

    public function store(QuestionRequest $request)
    {

        //if an image has been uploaded, move it to the appropriate directory, and save the image name as a variable for the table
        //TODO: Currently this just accepts the image at whatever the original size is.  Identify a max dimension and resize image appropriately
        if($request->file('picture') != '')
        {
            //get the uploaded image name and move it to the appropriate directory
            $imageName = Image::make($request->file('picture')->getRealPath());
            $path = public_path('img/questions/' . $request->file('picture')->getClientOriginalName());
//            $imageName->resize('200','200')->save($path);
            $imageName->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);
            $imageURL = $request->file('picture')->getClientOriginalName();

//            $request->file('picture')->move(base_path() . '/public/img/questions/', $imageName);

        }
        else
        {
            $imageURL = '';
        }


        //Create a new question using the request data + the imageName variable
        $question = new Question(
            array(
                'question' => $request->get('question'),
                'category' => $request->get('category'),
                'value' => $request->get('value'),
                'answer_a' => $request->get('answer_a'),
                'answer_b' => $request->get('answer_b'),
                'answer_c' => $request->get('answer_c'),
                'answer_d' => $request->get('answer_d'),
                'correct_answer' => $request->get('correct_answer'),
                'explanation' => $request->get('explanation'),
                'learning_point' => $request->get('learning_point'),
                'picture' => $imageURL
            )
        );
        if (Input::get('MD') == '1') {
            $question['MD'] = Input::get('MD');
        }
        if (Input::get('PA') == '1') {
            $question['PA'] = Input::get('PA');
        }
        if (Input::get('PT') == '1') {
            $question['PT'] = Input::get('PT');
        }
        if (Input::get('RX') == '1') {
            $question['RX'] = Input::get('RX');
        }
        if (Input::get('D') == '1') {
            $question['D'] = Input::get('D');
        }

        $question->save();

        flash('Your question was created.')->important();

        return redirect('questions');
    }

    public function edit($id)
    {
        //get all options for question categories for returning to the view and populating the dropdown
        $optionsObject = $this->getOptionsArray();
        $options = [];
        for($i=0;$i<count($optionsObject);$i++) {
            $options[] = $optionsObject[$i]->category;
        }

        //find the question by its ID to populate the form
        $question = Question::find($id);

        $edit = "edit";
        return view('questions.edit', compact('question', 'options', 'edit'));
    }

    public function update($id, QuestionRequest $request)
    {
        //find the question by its id, or the whole thing should fail
        $question = Question::findOrFail($id);

        //if a new picture file has been uploaded, take care of moving it to the right directory
        //TODO: Currently this just accepts the image at whatever the original size is.  Identify a max dimension and resize image appropriately
        if($request->file('picture') != null) {
            //get the original image path, if it exists
            $original = DB::table('questions')->where('id', $id)->pluck('picture');

            //get the uploaded image name and move it to the appropriate directory
            //$imageName = $request->file('picture')->getClientOriginalName();

            $imageName = Image::make($request->file('picture')->getRealPath());
            $path = public_path('img/questions/' . $request->file('picture')->getClientOriginalName());
            $imageName->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);

            $imageURL = $request->file('picture')->getClientOriginalName();

            //$request->file('picture')->move(base_path() . '/public/img/questions/', $imageName);

            //update the question's image field
            $question->update(array(
                'picture' => $imageURL
                )
            );

            //if there was an image in place before, go delete it now that the new one is in place
            if($original != '') {
                File::delete(base_path() . '/public/img/questions/' . $original);
            }
        }

        //Update the question using the request data
        $question->update(
            array(
                'question' => $request->get('question'),
                'category' => $request->get('category'),
                'value' => $request->get('value'),
                'answer_a' => $request->get('answer_a'),
                'answer_b' => $request->get('answer_b'),
                'answer_c' => $request->get('answer_c'),
                'answer_d' => $request->get('answer_d'),
                'correct_answer' => $request->get('correct_answer'),
                'explanation' => $request->get('explanation'),
                'learning_point' => $request->get('learning_point'),
            )
        );
        if (Input::get('MD') == '1') {
            $question['MD'] = Input::get('MD');
        }
        else{
            $question['MD'] = 0;
        }
        if (Input::get('PA') == '1') {
            $question['PA'] = Input::get('PA');
        }
        else{
            $question['PA'] = 0;
        }
        if (Input::get('PT') == '1') {
            $question['PT'] = Input::get('PT');
        }
        else{
            $question['PT'] = 0;
        }
        if (Input::get('RX') == '1') {
            $question['RX'] = Input::get('RX');
        }
        else{
            $question['RX'] = 0;
        }
        if (Input::get('D') == '1') {
            $question['D'] = Input::get('D');
        }
        else{
            $question['D'] = 0;
        }
        $question->save();

        flash('Your question was updated.')->important();

        return redirect('questions');
    }

    public function destroy($id)
    {
        //get the location of the associated image, if any, and delete it
        $imagePath = DB::table('questions')->where('id', $id)->pluck('picture');
        if($imagePath != '') {
            File::delete(base_path() . '/public/img/questions/' . $imagePath);
        }

        //get the question with the matching ID and destroy
        $question = Question::find($id);
        $question->destroy($id);

        flash('Your question was deleted.')->important();

        return redirect('questions');
    }

    public function uploadFromCSV(CSVRequest $request)
    {
        if($request->file('file') == NULL || $request->file('file' == '')) {
            flash('Please select a file to upload.')->important();
            return redirect('questions');
        }

        //get the name of the uploaded file and move it to the csv directory
        $uploadedFile = $request->file('file')->getClientOriginalName();
        $request->file('file')->move(base_path() . '/public/csv/', $uploadedFile);

        //if the user wants to empty the table, truncate
        if ($request->get('overwrite') == 1) {
            DB::table('questions')->truncate();
        }

        //Get the data from the CSV file and put it into the database using the seedFrom
        $seedData = $this->seedFromCSV(app_path() . '/../public/csv/' . $uploadedFile, ',');
        array_walk_recursive(
            $seedData,
            function (&$entry) {
                $entry = mb_convert_encoding($entry, 'UTF-8');
            }
        );
        for ($i = 0; $i < sizeof($seedData); $i++) {

//            $columns = array('question', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'explanation', 'learning_point');
//            for ($z = 0; $z < sizeof($columns); $z++) {
//                $string = htmlspecialchars($seedData[$i][$columns[$z]]);
//                $hexString = bin2hex($string);
//                $cleanString = preg_replace('/efbfbd/', '27', $hexString);
//                $seedData[$i][$columns[$z]] = hex2bin($cleanString);
//            }
            if (strtoupper($seedData[$i]['MD']) == 'N') {
                $seedData[$i]['MD'] = 0;
            } elseif (strtoupper($seedData[$i]['MD']) == 'Y') {
                $seedData[$i]['MD'] = 1;
            } elseif(strtoupper($seedData[$i]['MD']) == ''){
                $seedData[$i]['MD'] = 0;
            }

            if (strtoupper($seedData[$i]['PA']) == 'N') {
                $seedData[$i]['PA'] = 0;
            } elseif (strtoupper($seedData[$i]['PA']) == 'Y') {
                $seedData[$i]['PA'] = 1;
            } elseif (strtoupper($seedData[$i]['PA']) == '') {
                $seedData[$i]['PA'] = 0;
            }

            if (strtoupper($seedData[$i]['PT']) == 'N') {
                $seedData[$i]['PT'] = 0;
            } elseif (strtoupper($seedData[$i]['PT']) == 'Y') {
                $seedData[$i]['PT'] = 1;
            } elseif (strtoupper($seedData[$i]['PT']) == '') {
                $seedData[$i]['PT'] = 0;
            }

            if (strtoupper($seedData[$i]['RX']) == 'N') {
                $seedData[$i]['RX'] = 0;
            } elseif (strtoupper($seedData[$i]['RX']) == 'Y') {
                $seedData[$i]['RX'] = 1;
            } elseif (strtoupper($seedData[$i]['RX']) == '') {
                $seedData[$i]['RX'] = 0;
            }

            if (strtoupper($seedData[$i]['D']) == 'N') {
                $seedData[$i]['D'] = 0;
            } elseif (strtoupper($seedData[$i]['D']) == 'Y') {
                $seedData[$i]['D'] = 1;
            } elseif (strtoupper($seedData[$i]['D']) == '') {
                $seedData[$i]['D'] = 0;
            }
        }
        DB::table('questions')->insert($seedData);

        flash('The questions were successfully uploaded.')->important();

        return redirect('questions');
    }

    /**
     * Collect data from a given CSV file and return as array
     *
     * @param $filename
     * @param string $deliminator
     * @return array|bool
     */
    private function seedFromCSV($filename, $deliminator = ",")
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 4096, $deliminator)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }

            }
            fclose($handle);
        }

        return $data;
    }


    private function getOptionsArray()
    {
        $options = DB::table('questions')->select('category')->groupBy('category')->get();

        return $options;
    }

}