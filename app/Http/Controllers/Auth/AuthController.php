<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
//use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	//use AuthenticatesAndRegistersUsers;

    protected $redirectAfterLogout = 'auth/login';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	/*public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}*/

    public function getLogin()
    {
        return view('auth.login');
    }

   public function postLogin(Request $request){

       $ip = $_SERVER['REMOTE_ADDR'];
       $agent = $_SERVER['HTTP_USER_AGENT'];
       $timenow = gmdate('U') + 0;

       // Get current record if one exists.
       $loginAttemptRecordCollection = Login::where('ip', '=', $ip)->get();
       $max_allowed_login_attempts = 5;
       $lockout_period_in_seconds = 60*60; //1 hour

       // Delete any attempt record that is older than the lockout-period based from now
       $array_last_login_attempts =  ($timenow - $lockout_period_in_seconds);
       DB::table('logins')->where('last_login_attempt', '<',$array_last_login_attempts)->delete();

       if(!isset($loginAttemptRecordCollection[0])  || $loginAttemptRecordCollection[0]['number_login_attempts'] < 5){
           //validation block
           $this->validate($request, [
               'email' => 'required|email', 'password' => 'required',
           ]);

           $credentials = $request->only('email', 'password');

           if (Auth::attempt($credentials, $request->has('remember'))) {
               DB::table('logins')->where('ip', '=', $ip)->delete();
               return redirect()->intended($this->redirectPath());
           }
       }

       // Perform the authentication based on your requirements


       // If it doesn't exist, set some other key points for later creation.
       if (isset($loginAttemptRecordCollection[0])) {
           $loginAttemptRecord = $loginAttemptRecordCollection[0];
           $login = $loginAttemptRecord;

       } else {
           $loginAttemptRecord = array();
           $loginAttemptRecord['ip'] = $ip;
           $loginAttemptRecord['agent'] = $agent;
           $loginAttemptRecord['first_login_attempt'] = $timenow;
           $loginAttemptRecord['number_login_attempts'] = 0;
           $loginAttemptRecord['last_login_attempt'] = $timenow;
           $login = new Login($loginAttemptRecord);
       }

       $timeRemaining = ($login->last_login_attempt + $lockout_period_in_seconds) - $timenow; // -1 Per Ryan's Request

       if ($login->number_login_attempts >= $max_allowed_login_attempts) {
           // Throw forbidden; attempts exceeded.
           // Don't even update the table.
           // Also give back time remaining.
           if($timeRemaining < 0)
           {
               $timeRemaining = 0;
           }
           flash()->message("Login attempt blocked; exceeded max attempt limit of $max_allowed_login_attempts. Blocked for $timeRemaining seconds.", 'important');
       }

       else {
           // Else, up attempt count on login attempts and throw error.
           $login->last_login_attempt = $timenow;
           $login->number_login_attempts += 1;
           $login->save();

           $attemptsLeft = $max_allowed_login_attempts - ($login->number_login_attempts); // Login tries LEFT after THIS try.
           if ($attemptsLeft > 0) {
               // Let them know they can login more times.
               flash()->message("These credentials do not match our records. $attemptsLeft attempt(s) remaining.", 'important');
           }

           else {
               // No more login attempts allowed.
               // Reset time remaining to last attempt.
               $timeRemaining = ($loginAttemptRecord['last_login_attempt']  + $lockout_period_in_seconds) - $timenow; // -1 Per Ryan's Request
               flash()->message(" These credentials do not match our records. Blocked for $timeRemaining seconds.", 'important');
           }
       }

       return redirect($this->loginPath())
           ->withInput($request->only('email', 'remember'));
   }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginPath()
    {
        return '/auth/login';
    }


    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');

    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

}
