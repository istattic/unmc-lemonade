<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\AccountRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $users=  DB::select('select * from users');
        $loggedInUser = Auth::user();
        $loggedInUserId = $loggedInUser['id'];
        //return $loggedInUserId;
        return view('users.index' ,compact('users','loggedInUserId'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $create = "create";
        return view('users.create',compact('create'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserCreateRequest $request)
	{
        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password =  Hash::make($request->password);
        $user->role = $request->role;
        $user->save();

        flash('Your user was created.')->important();

        return redirect('users');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $user = User::find($id);
        return view('users.edit',compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UserEditRequest $request)
	{
        //find the user by its id, or the whole thing should fail
        $user = User::findOrFail($id);

        $user->username = $request->username;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->update();

        flash('Your user was updated.')->important();

        return redirect('users');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function changePassword($id)
    {

         //find the user by its id, or the whole thing should fail
        $user = User::findOrFail($id);
        return view('users.password', compact('user'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function changeAccountPassword($id)

    {
        //find the user by its id, or the whole thing should fail
        $user = User::findOrFail($id);
        return view('users.accountPassword', compact('user'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updatePassword($user_id, PasswordRequest $request)
    {
        //find the user by its id, or the whole thing should fail

        $user = User::findOrFail($user_id);
        $referPage = $request->input('hiddenReferField');
        if ($referPage != 'password') {
            $old_password = $user->password;
            if (Hash::check($request->old_password, $old_password)) {

                $user->password = Hash::make($request->password);
                $user->update();
            } else {
                flash()->message('Your old password did not match our records. Please try again.', 'important');
                return view('users.accountPassword', compact('user'));
            }
        }


        $loggedInUser = Auth::user();
        $loggedInUserId = $loggedInUser['id'];


        if ($referPage == 'password') {
            flash('The password was updated.')->important();
            return redirect('/users');
        } else {
            flash('Your password was updated.')->important();
            return redirect()->action('UsersController@manageAccount', ['id' => $loggedInUserId]);
        }

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        //get the user with the matching ID and destroy
        $user = User::find($id);
        $user->destroy($id);
        return response()->json(array('message' => 'The user was deleted.', 'id' => $id ));
	}

    /**
     * Show the form for editing the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function editAccount($id) {
        $user = User::findOrFail($id);
        return view('users.editAccount',compact('user'));
    }

    /**
     * Show the listing of session user information
     *
     * @param  int  $id
     * @return Response
     */
    public function manageAccount($id) {
        $user = User::findOrFail($id);
        return view('users.account',compact('user'));
    }

    /**
     * Update the specified account information in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateAccount($id, AccountRequest $request ) {
        $user = User::findOrFail($id);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->update();

        flash('Your information was updated.')->important();
        return view('users.account',compact('user'));
    }

}
