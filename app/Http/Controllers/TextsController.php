<?php namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Texts;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class TextsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Texts Controller
    |--------------------------------------------------------------------------
    |
    | Manages the admin user's back-end CRUD system for About/Resources text
    |
    |
    */

    public function index()
    {
        $sections = DB::table('texts')->get();

        return view('texts.index',
            compact(
                'sections'
            )
        );
    }

    public function show($id)
    {

        $section = Texts::find($id);
        return view('texts.show', compact('section'));
    }

    public function create()
    {
        return view ('texts.create');
    }

    public function store(QuestionRequest $request)
    {
        $texts = new Texts(
            array(
                'section_title' => $request->get('section_title'),
                'section_text' => $request->get('section_text'),
            )
        );

        $texts->save();

        flash('Your page section was created.')->important();

        return redirect('texts');
    }

    public function edit($id)
    {
        //find the question by its ID to populate the form
        $section = Texts::find($id);

        return view('texts.edit', compact('section'));
    }

    public function update($id, Request $request)
    {
        //find the text section by its id, or the whole thing should fail
        $text = Texts::findOrFail($id);

        //Update the question using the request data
        $text->update(
            array(
                'section_text' => $request->get('section_text'),
            )
        );

        flash('Your page section was updated.')->important();

        return redirect('texts');
    }

    public function destroy($id)
    {
        //
    }


}