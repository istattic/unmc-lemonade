<?php namespace App\Http\Controllers;

use App\Character;
use App\Game;
use App\Http\Requests;
use App\Question;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class StandController extends Controller
{


    public function start()
    {
        return view('stand.start');
    }

    public function selectShift()
    {
        $g = Game::loadState();
        $playerDiscipline = $g->getDiscipline();
        $options = DB::table('questions')->select('category')->groupBy('category')->get();

        // $optionsWithCount= array('category', 'questionCount');

        for ($i = 0; $i < count($options); $i++) {
            $questionsSeen = $g->getQuestionsSeen();
            $questionCount = count(DB::table('questions')->whereCategory($options[$i]->category)->where($playerDiscipline, '=', '1')->whereNotIn('id', $questionsSeen)->get());
            $optionsWithCount[$i]['category'] = strtolower($options[$i]->category);
            $optionsWithCount[$i]['questionCount'] = $questionCount;
        }

        //return view('stand.selectShift', compact('options'));
        return view('stand.selectShift', compact('options', 'optionsWithCount'));
    }

    public function startShift(Request $request)
    {
        $g = Game::loadState();

        $category = $request->input('category');

        $g->setShift($category);
        $g->saveState();

        return redirect('stand');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {


        //get the player's character id and find the doctor from that ID
        $g = Game::loadState();

        if (!$g->shifts) {
            return redirect('stand/selectShift');
        }

        $id = $g->getPlayerCharacter();
        // Refactor: return the actual object?
        // Refactor: rename getPc to getPCId ?
        //$id = Session::get('player_character');
        $doctor = Character::find($id);

        if (!last($g->getCurrentShiftQuestions()) || !array_key_exists('answered', last($g->getCurrentShiftQuestions()))) {
            if ($g->getCurrentShiftQuestions() != 0) {
                $g->unsetLastQuestion();
                $g->saveState();
                $lastQuestion = last($g->getCurrentShiftQuestions());
                $lastCharacter = $lastQuestion['patient'];
            } else {
                $lastQuestion = '';
                $lastCharacter = '';
            }
        } else {
            $lastQuestion = last($g->getCurrentShiftQuestions());
            $lastCharacter = $lastQuestion['patient'];
        }

        //get the count of how many patients have already been seen, for storing correctly in session array
        $patientsSeen = count($g->getCurrentShiftQuestions());
        $currentShift = last($g->shifts);

        //array of question ids already seen
        $questionsSeen = $g->getQuestionsSeen();
        $playerDiscipline = $g->getDiscipline();

        if ($currentShift['category'] == 'random') {
            $totalCategoryAndDisciplineQuestions = count(DB::table('questions')->where($playerDiscipline, '=', '1')->get());
        } else {
            //$totalCategoryQuestions = count(DB::table('questions')->whereCategory(strtoupper($currentShift['category']))->get());
            $totalCategoryAndDisciplineQuestions = count(DB::table('questions')->whereCategory(strtoupper($currentShift['category']))->where($playerDiscipline, '=', '1')->get());
        }

        //This checks whether the player has already answered all the questions for this category, in this shift or a previous one.
        $categoryQuestionsAlreadySeen = count(DB::table('questions')->whereCategory(strtoupper($currentShift['category']))->where($playerDiscipline, '=', '1')->whereIn('id', $questionsSeen)->get());

        if ($patientsSeen < 10 && $patientsSeen < $totalCategoryAndDisciplineQuestions && $categoryQuestionsAlreadySeen < $totalCategoryAndDisciplineQuestions) {
            //Assign arrays of all possible patients to assign to the questions
            //TODO:  Make this dynamic in case of changes to patient images, rather than static array
            $patientsArray = ['f-03', 'f-05', 'f-08', 'f-09', 'f-12', 'f-13', 'm-01', 'm-02', 'm-03', 'm-04', 'm-06', 'm-07', 'm-10', 'm-11', 'm-13'];
            $pregnantArray = ['p-06', 'p-15'];

            //Randomly grab a new question from all possible questions; keep doing this until a question comes up that hasn't been seen yet.
            $playerDiscipline = $g->getDiscipline();
            if ($currentShift['category'] == 'random') {
                //if they want random questions, don't check for category
                do {
                    $question = Question::all()->random(1);
                } while (in_array($question->id, $questionsSeen) || !($question->$playerDiscipline) == '1');
            } else {
                //if they want a specific category, check for both category match & whether the question has already been seen
                do {
                    $question = Question::all()->random(1);
                    $check = $question->$playerDiscipline;
                } while (in_array($question->id, $questionsSeen) || !($question->category == strtoupper($currentShift['category']) ));
            }

            //Getting the question ID out of the object; this will be stored in the session for "Next Patient"
            $questionID = $question->id;


            //Checking if a question relates to pregnancy, and ensuring that the selected patient is a pregnant character if it is; otherwise, select a non-pregnant character
            if ($question->category == 'PREGNANCY') {
                do {
                    $patientNo = 'patient-' . $pregnantArray[array_rand($pregnantArray)];
                } while ($patientNo == $lastCharacter);
            } else {
                do {
                    $patientNo = 'patient-' . $patientsArray[array_rand($patientsArray)];
                } while ($patientNo == $lastCharacter);
            }

            //Store these "Next Patient" questions in the session so that when the player proceeds to the question, they see the same character
            Session::put('nextQuestion.question', $questionID);
            Session::put('nextQuestion.patient', $patientNo);

            return view('stand.index', compact('doctor', 'patientsSeen', 'patientNo', 'questionID', 'lastCharacter', 'lastQuestion'));
        } else {
            //return $totalCategoryAndDisciplineQuestions;
            return redirect('stand/review');
        }

    }


    /**
     *  Display current question
     */
    public function question()
    {
        if(!Session::get('nextQuestion')) {
            return redirect()->action('StandController@answer');
        } else {
            $questionID = Session::get('nextQuestion.question');
            $question = Question::find($questionID);
        }


        // Get current game object.
        $g = Game::loadState();

        //tells us how many items are already in the array, so that new questions are placed appropriately in the array
        $count = count($g->getCurrentShiftQuestions());

//        //get the question ID from the "Next Question" session array and grab the whole question based on that
//        $questionID = Session::get('nextQuestion.question');
//        $question = Question::find($questionID);

        //get the "Patient" character stored in the "Next Question" session array and create the URL to their head image for display
        $patientNo = Session::get('nextQuestion.patient');
        $patientImgURL = $patientNo . '-head.png';


        //Now that the player has seen the question, store it to the "Questions" array in the session so that it is available in the Patients Summary
        $currentQuestion = [
            'question' => $question->toJson(),
            'patient' => $patientNo
        ];
        $g->addQuestion($currentQuestion);
        $g->addQuestionSeen($questionID);

        $g->saveState(); // assumes save to session

        return view('stand.question', compact('question', 'patientImgURL'));
    }

    /**
     *  Display current question
     */
    public function questionCheck(Request $request)
    {
        $g = Game::loadState();

        $questions = $g->getCurrentShiftQuestions();
        //tells us how many items are already in the array, in order to associate the answer given with the current question
        $current = count($questions) - 1;

        //Placing the player's answer in the array with the question, for later use in the Patients Summary
//        Session::put('questions.' . $current . '.id', $request->input('question_id')); // used??
        $g->setQuestionAnswered($current, $request->input('answer'));
//        Session::put('questions.' . $current . '.answered',$request->input('answer'));

        //Get the question based on its ID
        $question = Question::find($request->input('question_id'));

        //Check whether given answer matches the correct answer, and if it does add the question points to the player score
        if (strtoupper($question->correct_answer) == strtoupper($request->input('answer'))) {
            $g->addPoints($question->value);
        }

        //Add the question points to the possible score, regardless of whether it was answered correctly
        $g->increasePossibleScore($question->value);

        // Just added a new question.
        $g->saveState();

        Session::forget('nextQuestion');

        return redirect('stand/answer');
    }

    /**
     *  Display current Answer
     */
    public function answer()
    {
        $g = Game::loadState();
        //Get the question array from the session, and get the current (just answered) one
        $questions = $g->getCurrentShiftQuestions();
        $currentQuestion = last($questions);

        //get the player's answer and the assigned patient from the current question
        $answered = $currentQuestion['answered'];
        $patient = $currentQuestion['patient'];


        //decode the question json array
        $question = json_decode(($currentQuestion['question']));

        //Assign variables to a data array, to pass back to the view
        $data = ['question' => $question, 'answered' => $answered, 'patient' => $patient];

        return view('stand.answer', $data);
    }

    /**
     * Display review/billing page for all the patients seen by the player
     */
    public function review()
    {
        $g = Game::loadState();

        //get the total possible and earned scores for the player
        $totalEarned = $g->getEarnedScore();
        $possibleEarned = $g->getPossibleScore();

        if (count($g->getAllShiftQuestions()) == 0) {
            return redirect('stand.index');
        }

        $allQuestions = [];
        $correct = 0;
        $totalQuestions = 0;

        $i = 0;
        $s = 0;

        if (!isset($allQuestions['earned'])) {
            $allQuestions['earned'] = 0;
            $allQuestions['correct'] = 0;
        }
        if (!isset($allQuestions['potential'])) {
            $allQuestions['potential'] = 0;
            $allQuestions['total'] = 0;
        }
        $shift = $g->getAllShiftQuestions();
        $allQuestions['category'] = $shift['category'];

        foreach ($shift['questions'] as $q) {
            if (array_key_exists('answered', $q)) {
                $allQuestions['questions'][$i]['question'] = json_decode($q['question']);
                $allQuestions['questions'][$i]['answered'] = $q['answered'];
                $allQuestions['questions'][$i]['patient'] = $q['patient'];
                if (strtoupper($allQuestions['questions'][$i]['answered']) == strtoupper($allQuestions['questions'][$i]['question']->correct_answer)) {
                    $correct++;
                    $allQuestions['earned'] = $allQuestions['earned'] + $allQuestions['questions'][$i]['question']->value;
                    $allQuestions['correct']++;
                }
                $allQuestions['potential'] = $allQuestions['potential'] + $allQuestions['questions'][$i]['question']->value;
                $allQuestions['total']++;
                $totalQuestions++;
            }
            $i++;
        }

        return view('stand.review', compact('allQuestions', 'possibleEarned', 'totalEarned'));
    }

    public function previousReview()
    {
        $g = Game::loadState();
        $currentShift = last($g->getCurrentShiftQuestions());
        return view('stand.previousReview', compact('currentShift'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
