<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class AuthId {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $id = $request->route()->parameter('id');

        if(!Session::has('id')) {
            flash()->message('Your session timed out. Please log in again.','important');
            return redirect('/auth/login');
        }
        if(isset($this->auth->user()->id)) {
            if ($id != $this->auth->user()->id) {

                flash()->message('You are not permitted to complete that action.  If you are an administrator and would like to update another user, go to Manage Users.', 'important');
                return redirect('/home');
            }
        } else {
            flash()->message('Please login first to view that portion of the site.', 'important');
            return redirect('/auth/login');
        }
        return $next($request);
    }

}
