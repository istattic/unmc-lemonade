<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Session;

class AuthRole {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
	{
        if(!Session::has('id')) {
            flash()->message('Please login to view this page.');
            return redirect('/auth/login');
        }

        if(isset($this->auth->user()->role)) {
            if ($this->auth->user()->role != 's') {
                flash()->message('You got redirected to home page because you do not have permission to manage that portion of the site. Please choose one of the options below', 'important');
                return redirect('/home');
            }
        }else {
            flash()->message('Please login first to view that portion of the site.', 'important');
            return redirect('/auth/login');
        }

        return $next($request);
	}

}
