<?php namespace App\Http\Middleware;

use App\Game;
use Closure;
use Illuminate\Support\Facades\Session;

class StandSessionExists {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

        if(! Session::has('game_state')) {
            return redirect('game/characters');
        }

		return $next($request);
	}

}
