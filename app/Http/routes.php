<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Http\Controllers\GameController;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', 'GameController@index');
Route::get('game/characters', 'GameController@characters');
Route::post('game/characters', 'GameController@characterSelect');
Route::get('game/gameInstructions', 'GameController@gameInstructions');


$router->group(['middleware' => 'stand'], function () {
    Route::get('stand/', 'StandController@index');
    Route::get('stand/question', 'StandController@question');
    Route::post('stand/question', 'StandController@questionCheck');
    Route::get('stand/answer', 'StandController@answer');
    Route::get('stand/review', 'StandController@review');
    Route::get('stand/previousReview', 'StandController@previousReview');
    Route::get('game/finalReview', 'GameController@finalReview');
    Route::get('game/fullQuestionList', 'GameController@fullQuestionList');
    Route::get('game/endGame', 'GameController@endGame');
    Route::get('stand/selectShift','StandController@selectShift');
    Route::post('stand/selectShift', 'StandController@startShift');
});

Route::get('game/highScores', 'GameController@highScores');

Route::get('game/fullQuestionList', 'GameController@fullQuestionList');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::post('auth/logout', 'Auth\AuthController@getLogout');



$router->group(['middleware' => 'auth'], function () {
    Route::resource('questions', 'QuestionsController');
    Route::post('questions/uploadFromCSV', 'QuestionsController@uploadFromCSV');
    Route::resource('users', 'UsersController');
    Route::get('users/{id}/account', 'UsersController@manageAccount');
    Route::get('users/{id}/editAccount', 'UsersController@editAccount');
    Route::post('users/{id}/editAccount', 'UsersController@updateAccount');
    Route::post('users/{id}/destroy', 'UsersController@destroy');
    Route::post('users/{id}/password', 'UsersController@updatePassword');
    Route::get('users/{id}/password', 'UsersController@changePassword');
    Route::get('users/{id}/account/changeMyPassword', 'UsersController@changeAccountPassword');
    Route::resource('texts', 'TextsController');

});

$router->group(['middleware' => 'role'], function () {
    Route::resource('users', 'UsersController');
    Route::post('users/{id}/destroy', 'UsersController@destroy');
    Route::get('users/{id}/password', 'UsersController@changePassword');
    Route::resource('texts', 'TextsController');
    Route::get('/texts', 'TextsController@index');
    Route::get('texts/{id}/edit', 'TextsController@edit');
});

$router->group(['middleware' => 'id'], function () {
    Route::get('users/{id}/editAccount', 'UsersController@editAccount');
    Route::get('users/{id}/account', 'UsersController@manageAccount');
    Route::get('users/{id}/changeMyPassword', 'UsersController@changeAccountPassword');
});


$router->get('downloadCSV', function () {
    $tableData = DB::table('questions')->get(array('question', 'category', 'value', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer', 'explanation', 'learning_point', 'picture', 'MD', 'PT', 'PA', 'RX', 'D'));
    foreach ($tableData as $column)
    {
        if($column->MD == 0)
        {
            $column->MD = 'N';
        }
        elseif($column->MD == 1)
        {
            $column->MD = 'Y';
        }

        if($column->PA == 0)
        {
            $column->PA = 'N';
        }
        elseif($column->PA == 1)
        {
            $column->PA = 'Y';
        }

        if($column->PT == 0)
        {
            $column->PT = 'N';
        }
        elseif($column->PT == 1)
        {
            $column->PT = 'Y';
        }

        if($column->RX == 0)
        {
            $column->RX = 'N';
        }
        elseif($column->RX == 1)
        {
            $column->RX = 'Y';
        }

        if($column->D == 0)
        {
            $column->D = 'N';
        }
        elseif($column->D == 1)
        {
            $column->D = 'Y';
        }
    }
    foreach ($tableData as $datum) {
        $datum = (array)$datum;
        $dataArray[] = $datum;
    }

    $excel = App::make('excel');

    Excel::create('questions', function ($excel) use ($dataArray) {
        $excel->setTitle('Lemonade Stand Question Bank');

        $excel->sheet('Questions', function ($sheet) use ($dataArray) {
            $sheet->fromArray($dataArray, null, 'A1', false, true);
        });
    })->download('csv');
});


Route::get('home', 'HomeController@index');
Route::get('about', 'GameController@about');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
