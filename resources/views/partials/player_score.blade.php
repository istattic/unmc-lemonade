<div class="player_score">
    <div class="clearfix ">
        <h5 class="possible_score">Possible: ${{ $possible_score }}</h5>
        <span class="earned_score">Earned: ${{ $earned_score }}</span>
    </div>
</div>