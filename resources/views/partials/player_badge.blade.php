<div class="player_badge">
    <div class="player_icon">
        <img class="" src="{{ asset($player_character_img_url) }}">
    </div>
    <div class="clearfix">
        <h5 class="player_name">{{ $current_user }}</h5>
        <span class="player_discipline">{{ $discipline }}</span>
    </div>
</div>