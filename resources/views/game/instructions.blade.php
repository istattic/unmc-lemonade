@extends('app')

@section('content')

    <!-- Slides Container -->
    <div id="slider1_container" class="row" style="position: relative; top: 0px; left: 0px; width: 1140px; height: 800px; padding-left:2px; padding-right:2px; margin: auto auto;">
        <div u="slides" class="row" style="cursor: move; position: absolute; overflow: hidden; top: 40px; width: 1140px; height: 800px;">
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Character_setup_1.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Character_setup_2.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Seeing_patients_3.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Seeing_patients_4.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Seeing_patients_5.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Review_results_7.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image" src="{{asset('/img/how_to_play/Ending_game_6.png')}}" />
            </div>
            <div>
                <img u="image" class="slider-image final-review-slider-image" src="{{asset('/img/how_to_play/finalreview_2.png')}}" />
            </div>

            <!-- Arrow Left -->
            <span u="arrowleft" class="jssora02l">
            </span>
                <!-- Arrow Right -->
            <span u="arrowright" class="jssora02r">
            </span>
        </div>

    </div>
    <!-- End of Slides Container -->

    <div class="startGame col-md-1 col-md-offset-5">
        <a href="{{ url('/game/characters') }}" class="btn btn-primary text-right">Start Game</a>
    </div>



    <script>
        jQuery(document).ready(function ($) {
            var _CaptionTransitions = [];
            _CaptionTransitions["L"] = { $Duration: 800, x: 0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
            _CaptionTransitions["R"] = { $Duration: 800, x: -0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };

            var options = {
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,
                    $ChanceToShow: 2,
                    $AutoCenter: 2
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$,
                    $ChanceToShow: 2,
                    $AutoCenter: 1
                },
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlider$,
                    $CaptionTransitions: _CaptionTransitions,
                    $PlayInMode: 1,
                    $PlayOutMode: 1
                }
            };
            var jssor_slider1 = new $JssorSlider$('slider1_container', options);
        });
    </script>

@endsection