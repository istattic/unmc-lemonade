@extends('app')

@section('content')
    <div class="hide-small">
        {!! Form::open() !!}
        <div class="row">
            <div class="col-md-12 character_header">
                <h1>Who Are You?</h1>

                <h2>{!! Form::label('player_name','Your name: ') !!}
                    {!! Form::text('player_name',null, ['placeholder' => 'Enter your name', 'required', 'class' =>
                    'textbox_size']) !!}</h2>
                {{--<br>--}}
            </div>
        </div>
        <div class="character_select character_body row">
            <h2>Your Character:</h2>
            <?php $count = 1; ?>
            @foreach($chars as $c)
                <div class="col-sm-3 col-md-3 text-center character-choice">
                    <label for="doctor-{{ $c->id }}">
                        <img class="doctor-full-body center-block"
                             src="{{asset('/img/characters/doctors/doctor-')}}{{ $c->gender }}-0{{ $c->number }}.png">
                    </label>

                    <div class="answer_radio">
                        {!! Form::radio('doctor', $c->id, false,['id' => 'doctor-' . $c->id, 'required']) !!}
                    </div>
                </div>
                <?php $count++; ?>
            @endforeach
        </div>
        <div class="row selection_row">
            <h2>Your Profession:</h2>

            <div class="col-md-2 text-center col-md-offset-1">
                <h3 class="discipline-label">Medical Doctor</h3>
                <button type="submit" name="discipline" value="MD"
                        class="btn btn-primary" {{ $chars[0]->active ? '' : 'disabled' }}>Choose
                </button>
            </div>
            <div class="col-md-2 text-center">
                <h3 class="discipline-label">Physical Therapist</h3>
                <button type="submit" name="discipline" value="PT"
                        class="btn btn-primary" {{ $chars[1]->active ? '' : 'disabled' }}>Choose
                </button>
            </div>
            <div class="col-md-2 text-center">
                <h3 class="discipline-label">Physician's Assistant</h3>
                <button type="submit" name="discipline" value="PA"
                        class="btn btn-primary" {{ $chars[2]->active ? '' : 'disabled' }}>Choose
                </button>
            </div>
            <div class="col-md-2 text-center">
                <h3 class="discipline-label">Pharmacist</h3>
                <button type="submit" name="discipline" value="RX"
                        class="btn btn-primary" {{ $chars[3]->active ? '' : 'disabled' }}>Choose
                </button>
            </div>
            <div class="col-md-2 text-center">
                <h3 class="discipline-label">Dentist</h3>
                <button type="submit" name="discipline" value="D"
                        class="btn btn-primary" {{ $chars[3]->active ? '' : 'disabled' }}>Choose
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <div class="row selection_row phone-message-hidden">
        <h4>This game works best if played on a tablet or computer. Please switch to another device to play!</h4>
    </div>

    <script>
        $(document).ready(function () {

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-grey',
                radioClass: 'iradio_square-grey',
                increaseArea: '20%'
            });
        });

    </script>

@endsection