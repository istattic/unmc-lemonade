@extends('app')

@section('content')
    <h1>Questions</h1>
    <hr>

    <div class="accordion vertical">

        @foreach($allCategoriesWithQuestions as $category)
            <section id="{{ $category['category'] }}">
                <a href="#{{ $category['category'] }}"><span><h2>{{ strtoupper($category['category']) }}</h2></span></a>

                <p>
                    @foreach($category['questions'] as $question)
                        <span>
                            <h4>
                                Question No. {{ $question->id }}
                            </h4>

                            <div class="body">
                                <b>Question Text:</b> {{ $question->question }}
                                <br/>
                                <b>Correct Answer:</b> {{ $question->correct_answer }}
                                ({{ $question->{'answer_' . strtolower($question->correct_answer)} }})
                                <br/>
                                <b>Explanation:</b> {{ $question->explanation }}
                                <br/>
                                <b>Learning Point:</b> {{ $question->learning_point }}
                            </div>
                        </span>
                        <br/>
                    @endforeach
                </p>
            </section>
        @endforeach

        <div class="col-md-2 col-md-offset-10">
            <a href="{{ url('/game/endGame') }}" class="btn btn-primary text-right">Back to Main Menu <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>
    </div>

@stop