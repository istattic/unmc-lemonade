@extends('app')

@section('content')
    <!-- Main component for a primary marketing message or call to action -->
    <div class="row character_header">
        <div class="col-sm-12 col-md-12 text-center">
            <h1>Rheumatology Remedy</h1>
        </div>
    </div>
    <div class="row selection_row row_height hide-small">
        <div class="col-md-12 text-center">
            <img class="main_doctor" src="{{asset('/img/characters/doctors/doctor-m-01-alt-2.png')}}">
            <ul class="list-unstyled game_menu">
                <li><a class="btn btn-lg btn-primary game_menu_button" href="{{ url('/game/characters') }}">Start Game</a><br><br>
                </li>
                <li><a class="btn btn-lg btn-info game_menu_button" href="{{ url('/game/gameInstructions') }}">How to Play</a><br>
                </li>
                <li><a class="btn btn-lg btn-info game_menu_button" href="{{ url('/game/highScores') }}">High Scores</a></li>
            </ul>
        </div>
    </div>

    <br/>

    <div class="panel panel-default row">
        <div class="panel-heading">About this Project</div>

        <div class="panel-body">
            {!! $about->section_text !!}
        </div>
    </div>

    <div class="row selection_row phone-message-hidden">
        <h4>This game works best if played on a tablet or computer.  Please switch to another device to play!</h4>
    </div>

@endsection