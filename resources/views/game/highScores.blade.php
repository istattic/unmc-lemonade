@extends('app')

@section('content')
    <div class="row question_header">
        <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-offset-2 text-center">
            <h1>Top 20 High Scores</h1>
        </div>
    </div>

    <div class="row leaderboard-row">
        <div class="col-sm-12 col-md-12">
            <div class="leaderboard-record">
                <div class="col-sm-1 col-md-1 col-md-offset-1 text-center leaderboard-header">
                       Rank
                </div>
                <div class="col-sm-4 col-md-4 text-center leaderboard-header">
                    Character
                </div>
                <div class="col-sm-3 col-md-3 leaderboard-header">
                    Name
                </div>
                <div class="col-sm-3 col-md-3 leaderboard-header">
                    Score
                </div>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>

    <?php $i = 1; ?>
    @foreach($scores as $score)

        <div class="row leaderboard-row">
            <div class="col-sm-12 col-md-12">
                <div class="leaderboard-record">
                    <div class="col-sm-1 col-md-1 col-md-offset-1 leaderboard-number">
                        <div class="numberCircle">
                            @if($i < 10)
                                <span class="number-text">{{ $i }}</span>
                            @else
                                <span class="number-text-two-digit">{{ $i }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <img class="leaderboard-head center-block" src="{{asset('')}}{{ $score->character }}" class="center-block">
                    </div>
                    <div class="col-sm-3 col-md-3 record-text">
                        <span class="record-name">{{ $score->username }}</span>
                    </div>
                    <div class="col-sm-3 col-md-3 record-text">
                        <span class="record-name">${{ $score->score }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
        <?php $i++; ?>
    @endforeach

    <div class="row selection_row">
        <div class="col-sm-2 col-sm-offset-9 col-md-2 col-md-offset-9">
            <a href="{{ url('/') }}" class="btn btn-primary text-right">Return to Home <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>
    </div>
@endsection