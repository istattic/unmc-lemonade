@extends('app')

@section('content')
    <div class="row question_header">
        <div class="col-sm-8 col-md-9">
            <h1>Final Review</h1>

            <h3>You earned ${{ $score }} out of ${{ $possible }}</h3>

            @if($totalQuestions > 0)
                <h3>You answered {{ $correct }} out of {{ $totalQuestions }} questions correctly.
                    ({{ floor(($correct/$totalQuestions)*100) }}% Correct)</h3>
            @else
                <h3>You answered {{ $correct }} out of {{ $totalQuestions }} questions correctly.
                    (0% Correct)</h3>
            @endif

            <br/>

            @if($score >= 10000)
                <h3>View All Questions&emsp;<a class="btn btn-primary" href="{{ url('/game/fullQuestionList') }}">View Questions</a></h3>
                <p>Because you earned at least $10,000 during your game, you are able to view the full question list!</p>
            @else
                <h3>View All Questions</h3>
                <p>You must earn at least $10,000 to view the full
                question list.</p>
            @endif
        </div>
        <div class="col-sm-4 col-md-3">
            @include('partials.player_badge')
        </div>
        <div class="col-sm-2 col-md-2 col-sm-offset-10 col-md-offset-10">
            <a href="{{ url('/game/endGame') }}" class="btn btn-primary text-right pull-right">End Game <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>
    </div>

    @foreach($allQuestions as $shift)
        @if(strtoupper($shift['category']) == 'RANDOM')
            <?php $category = "GRAB BAG"; ?>
        @else
            <?php $category = strtoupper($shift['category']); ?>
        @endif
        <div class="row question_text_area">
            SHIFT CATEGORY: {{ $category }}
            <br/>
            You earned ${{ $shift['earned'] }} out of ${{ $shift['potential'] }}
            <br/>
            You answered {{ $shift['correct'] }} out of {{ $shift['total'] }} questions correctly for this shift.
            @if($shift['total'] > 0)
                ({{ floor(($shift['correct']/$shift['total'])*100) }}% Correct)
            @else
                (0% Correct)
            @endif
        </div>
        @if($shift['total'] != 0)
            <?php
                //some quick counting for the <hr> lines between patients
                $i = 1;
                $total = count($shift['questions']);
            ?>
            @foreach($shift['questions'] as $question)
                <div class="row review-patients-row">
                    <div class="col-md-6 text-left">
                        <div class="category_header">
                    <span class="question_patient_icon">
                        @if (strtolower($question['question']->correct_answer) == strtolower($question['answered']))
                            <img src="{{asset('/img/characters/patients\/')}}{{$question["patient"]}}.png" class="center-block">
                        @else
                            <img src="{{asset('/img/characters/patients\/')}}{{$question["patient"]}}-alt.png"
                                 class="center-block">
                        @endif
                    </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="category_header">
                                <div class="col-md-10">
                                    <div>
                                        <b>Category:</b> {{ $question['question']->category }}
                                    </div>
                                    <br/>

                                    <div>
                                        <b>Possible Score:</b> ${{ $question['question']->value }}
                                        <br/>
                                        <b>Earned:</b>
                                        @if (strtolower($question['question']->correct_answer) == strtolower($question['answered']))
                                            ${{ $question['question']->value }}
                                        @else
                                            $0
                                        @endif
                                    </div>
                                    <br/>
                                </div>
                                <div class="col-md-10">
                                    <div class="category_header">
                                        <b>Patient Question:</b> {{ $question['question']->question }}
                                    </div>
                                    <br/>

                                    <div>
                                        <b>Correct Answer:</b> {{ $question['question']->correct_answer }}
                                        ({{ $question['question']->{'answer_' . strtolower($question['question']->correct_answer)} }}
                                        )
                                        <br/>
                                        <b>You Answered:</b> {{ strtoupper($question['answered']) }}
                                        ({{ $question['question']->{'answer_' . strtolower($question['answered'])} }})
                                    </div>
                                    <br/>

                                    <div>
                                        <b>Learning Point:</b> {{ $question['question']->learning_point }}
                                    </div>
                                </div>
                                <div class="col-md-2 ">

                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>

                </div>
                <div class="row review-patients-row">
                    @if($i != $total)
                        <hr/>
                    @endif

                </div>
                <?php $i++; ?>
            @endforeach
        @else
            <div class="row review-patients-row">
                <div class="col-md-12">
                    <h4>You didn't see any patients this shift.</h4>
                </div>

            </div>
        @endif
    @endforeach
    <div class="row question_header">
        <br/><br/>
        @if($score >= 10000)
            <h3>View All Questions</h3>

            <p>Because you earned at least $10,000 during your game, you are able to view the full question list!</p>

            <a class="btn btn-primary" href="{{ url('/game/fullQuestionList') }}">View All Questions</a>
        @else
            <h3>View All Questions</h3>

            <p>You must earn at least $10,000 to view the full
                question list.</p>
        @endif
        <div class="col-sm-2 col-md-2 col-md-offset-10">
            <a href="{{ url('/game/endGame') }}" class="btn btn-primary text-right">End Game <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>
    </div>
@endsection