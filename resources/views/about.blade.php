@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">About this Project</div>

                    <div class="panel-body">
                        {!! $about->section_text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Resources Consulted</div>

                    <div class="panel-body">
                        {!! $resources->section_text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Project Funding</div>

                    <div class="panel-body">
                        Funding provided by an award from the Office of the Vice-Chancellor for Academic Affairs at the
                        University of Nebraska Medical Center, with additional funding provided by the Division of
                        Rheumatology at Nebraska Medicine.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Development</div>

                    <div class="panel-body">
                        <p>
                            Programming, design, and development for this site was done by students and staff at the
                            <a href="http://attic.ist.unomaha.edu/" target="_blank">IS&T Attic</a>
                            in partnership with the University of Nebraska Medical Center.
                            The IS&T Attic is an in-house development group at the University of Nebraska at Omaha's
                            College of Information
                            Science and Technology (IS&T), and part of the university's Center for Management of
                            Information Technology (CMIT). For more information about the College of IS&T and its
                            programs, please visit <a href="http://ist.unomaha.edu" target="_blank">http://www.ist.unomaha.edu</a>.
                            <br/><br/>
                            <img class="ist-logo-image col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2"
                                 src="{{asset('/img/CIST-Horizontal-Black.png')}}"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection