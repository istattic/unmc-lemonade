@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>

                    <div class="panel-body">
                        You are logged in!
                        <br/>
                        <br/>
                        <a href="{{ action('QuestionsController@index') }}">Manage Questions</a>
                        @if(Auth::user()->role == 's')
                        <br/>
                            <a href="{{ action('UsersController@index') }}">Manage Users</a>
                        <br/>
                            <a href="{{ action('TextsController@index') }}">Manage Site Text</a>
                        @endif
                        <br/>
                        <a href="{{ action('UsersController@manageAccount',[Auth::user()->id]) }}">Manage My Account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
