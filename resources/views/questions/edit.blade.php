@extends('app')

@section('content')
    <h1>Edit Question No. {{ $question->id }}</h1>
    <div class="col-md-6 col-md-offset-3">
        @include('errors.list')
    {!! Form::model($question, ['method' => 'PATCH', 'action' => ['QuestionsController@update', $question->id], 'files' => true]) !!}

    @include('questions.form', ['submitButtonText' => 'Update Question'])

    {!! Form::close() !!}
    </div>

    <br>
    <br>


@stop