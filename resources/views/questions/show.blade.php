@extends('app')

@section('content')
    <div>
        <section>
            <h2>
                Question No. {{ $question->id }}
            </h2>
        <span style="font-size:smaller">
            {{ $question->category }}
            <br/>
            {{ $question->disciplines }}
        </span>
            <br/><br/>

            <div class="body">
                <b>Patient Question:</b>
                <br/>
                {{ $question->question }}
                <br/><br/>

                <b>Disciplines:</b>
                <br/>
                @if ( $question->MD == 1)
                <p>MD</p>
                @endif
                @if ( $question->PA == 1)
                    <p>PA</p>
                @endif
                @if ( $question->PT == 1)
                    <p>PT</p>
                @endif
                @if ( $question->RX == 1)
                    <p>RX</p>
                @endif
                @if ( $question->D == 1)
                    <p>D</p>
                @endif
                <br/>
                <dl>
                    <dt>Answer Options</dt>
                    <dd>
                        A. {{ $question->answer_a }}
                    </dd>
                    <dd>
                        B. {{ $question->answer_b }}
                    </dd>
                    <dd>
                        C. {{ $question->answer_c }}
                    </dd>
                    <dd>
                        D. {{ $question->answer_d }}
                    </dd>
                    <br/>
                    <dt>Correct Answer</dt>
                    <dd>
                        {{ $question->correct_answer }}
                    </dd>
                </dl>

                <br/>

                <b>Question Explanation:</b>
                {{ $question->explanation }}

                <br/><br/>

                <br/><br/>

                <b>Question Value:</b> ${{ $question->value }}

                <br/><br/>
                @if ($question->picture != '')
                    <div class="question_image text-center">
                        <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox"><img
                                    src="{{ asset('/img/questions\/')}}{{ $question->picture }}" class="thumbnail center-block"></a>
                        <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox" class="btn btn-info">View Larger
                            Image</a>
                    </div>
                @endif
                <br/>

            </div>
        </section>
    </div>
    <br/>
    <div class="col-md-12">

        {!! Form::model($question, ['method' => 'DELETE', 'action' => ['QuestionsController@destroy', $question->id],
        'files' => true]) !!}
        <div class="form-group">
            {!! Form::submit('Delete Question', ['class' => 'pull-right btn btn-default ']) !!}
        </div>
        {!! Form::close() !!}

        <a class="btn btn-default" href="{{ action('QuestionsController@index') }}">Return to Question List</a>

        <a class="btn btn-default" href="{{ action('QuestionsController@edit', [$question->id]) }}">Edit Question</a>
    </div>


@stop