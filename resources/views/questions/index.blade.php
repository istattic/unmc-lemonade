@extends('app')

@section('content')
    <h1>Questions</h1>
    <hr>

    <div class="accordion vertical">

        @foreach($allCategoriesWithQuestions as $category)
            <section id="{{ $category['category'] }}">
                <a href="#{{ $category['category'] }}"><span><h2>{{ strtoupper($category['category']) }}</h2></span></a>

                <p>
                    @foreach($category['questions'] as $question)
                        <span>
                            <h4>
                                <a href="{{ action('QuestionsController@show', [$question->id]) }}">Question
                                    No. {{ $question->id }}</a>
                            </h4>

                            <div class="body">
                                <b>Question Text:</b> {{ $question->question }}
                                <br/>
                                <b>Correct Answer:</b> {{ $question->correct_answer }}
                                ({{ $question->{'answer_' . strtolower($question->correct_answer)} }})
                                <br/>
                                <b>Explanation:</b> {{ $question->explanation }}
                                <br/>
                                <b>Learning Point:</b> {{ $question->learning_point }}
                            </div>
                            <br/>
                            <a class="btn btn-default" href="{{ action('QuestionsController@edit', [$question->id]) }}">Edit Question</a>
                        </span>
                        <br/>
                        <br/>
                    @endforeach
                </p>
            </section>
        @endforeach

        <div style="clear:both;"><br/></div>
        <div>
            <a class="btn btn-default" href="{{ action('QuestionsController@create', [$question->id]) }}">Add a New
                Question</a>
        </div>

    </div>

    <br/>

    <div class="grey-box">
        <h3>Upload Questions from CSV</h3>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#aboutCSV">
            CSV Instructions & Sample
        </button>
        <br/><br/>

        {!! Form::open(['url' => 'questions/uploadFromCSV', 'files' => true]) !!}

        <div class="form-group">
            {!! Form::label('file', 'Choose a CSV file to upload.') !!}
            {!! Form::file('file', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('overwrite', 1, true, ['class' => 'l']) !!}
            {!! Form::label('overwrite', 'Overwrite Existing Questions') !!}
            <br/>
            <small><i>Unchecking this box will add questions without removing those existing.</i></small>
        </div>

        <button class="btn btn-default" type="submit" value="submit">Upload File</button>

        {!! Form::close() !!}
    </div>

    <br/>

    <div class="grey-box">
        <h3>Download Questions to CSV</h3>

        <a class="btn btn-default" href="{{ url('/downloadCSV') }}">Download Questions</a>
    </div>

    {{--Modal div for CSV information--}}
    <div class="modal fade" id="aboutCSV">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Uploading Questions from CSV</h4>
                </div>
                <div class="modal-body">
                    <p>
                        A sample CSV file can be downloaded <a href="{{ url('/csv/sample/sample.csv') }}" download>here</a>. Please note that when using this
                        CSV as a template, the top row (header) <i>must</i> remain unchanged.
                    </p>

                    <p>
                        The question coumns and formats are as follows.
                    </p>

                    <dl>
                        <dt>
                            Question
                        </dt>
                        <dd>
                            This is the patient's description of symptoms and/or question for the doctor to answer.
                        </dd>
                        <dt>
                            Category
                        </dt>
                        <dd>
                            The category that this question applies to; for example, GOUT, CONTRAINDICATIONS/INTERACTIONS, PREGNANCY, or IMAGE. Each question can
                            be assigned only <i>one</i> category.
                        </dd>
                        <dt>
                            Value
                        </dt>
                        <dd>
                            The dollar amount that this question is worth in the game. Only numbers are accepted; do not
                            include "$" or decimal points.
                        </dd>
                        <dt>
                            Answer A
                        </dt>
                        <dd>
                            The first answer option.
                        </dd>
                        <dt>
                            Answer B
                        </dt>
                        <dd>
                            The second answer option.
                        </dd>
                        <dt>
                            Answer C
                        </dt>
                        <dd>
                            The third answer option.
                        </dd>
                        <dt>
                            Answer D
                        </dt>
                        <dd>
                            The fourth answer option
                        </dd>
                        <dt>
                            Correct Answer
                        </dt>
                        <dd>
                            The <i>letter</i> corresponding to the correct answer to the question.
                        </dd>
                        <dt>
                            Explanation
                        </dt>
                        <dd>
                            This is an explanation of which answer is correct, and why.
                        </dd>
                        <dt>
                            Learning Point
                        </dt>
                        <dd>
                            This is a brief takeaway concept relating to the question, intended to help the student
                            understand and retain core concepts.
                        </dd>
                        <dt>
                            Picture
                        </dt>
                        <dd>
                            This is the name of the picture file, if any, that is associated with the question. Please
                            note that only questions with the category of IMAGE can have a picture file; additionally,
                            after uploading the CSV, you will need to edit each image question individually to upload
                            the correct image file.
                        </dd>
                        <dt>
                            MD
                        </dt>
                        <dd>
                            This indicates whether the question should be shown for students who choose the discipline of Medical Doctor.  Accepted values are Y or N.
                        </dd>
                        <dt>
                            PT
                        </dt>
                        <dd>
                            This indicates whether the question should be shown for students who choose the discipline of Physical Therapist.  Accepted values are Y or N.
                        </dd>
                        <dt>
                            PA
                        </dt>
                        <dd>
                            This indicates whether the question should be shown for students who choose the discipline of Physician's Assistant.  Accepted values are Y or N.
                        </dd>
                        <dt>
                            RX
                        </dt>
                        <dd>
                            This indicates whether the question should be shown for students who choose the discipline of Pharmacist.  Accepted values are Y or N.
                        </dd>
                        <dt>
                            D
                        </dt>
                        <dd>
                            This indicates whether the question should be shown for students who choose the discipline of Dentist.  Accepted values are Y or N.
                        </dd>
                    </dl>

                </div>
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@stop