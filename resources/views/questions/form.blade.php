<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::textarea('question', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::input('category', 'category', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group" style="display: inline-block;">
    {!! Form::label('disciplines', 'Disciplines:') !!}
    <br>
    <br>

    <div class="form-group col-md-10">
        <p>{!! Form::checkbox('MD', '1', null) !!} Medical Doctor</p>
    </div>

    <div class="form-group col-md-10">
        <p>{!! Form::checkbox('PA', '1', null) !!} Physical Assistant</p>
    </div>

    <div class="form-group col-md-10">
        <p>{!! Form::checkbox('PT', '1', null) !!} Physical Therapist</p>
    </div>

    <div class="form-group col-md-10">
        <p>{!! Form::checkbox('RX', '1', null) !!} Pharmacist</p>
    </div>

    <div class="form-group col-md-10">
        <p>{!! Form::checkbox('D', '1', null) !!} Dentist</p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('value', 'Dollar Value:') !!}
    {!! Form::input('value', 'value', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer_a', 'Answer A:') !!}
    {!! Form::input('answer_a', 'answer_a', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer_b', 'Answer B:') !!}
    {!! Form::input('answer_b', 'answer_b', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer_c', 'Answer C:') !!}
    {!! Form::input('answer_c', 'answer_c', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer_d', 'Answer D:') !!}
    {!! Form::input('answer_d', 'answer_d', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('correct_answer', 'Correct Answer (Letter):') !!}
    {!! Form::input('correct_answer', 'correct_answer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('explanation', 'Answer Explanation:') !!}
    {!! Form::textarea('explanation', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('learning_point', 'Learning Point:') !!}
    {!! Form::textarea('learning_point', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('picture', 'Picture') !!}
    @if (isset($edit) && $question->picture != '')
        <div class="question_image text-center">
            <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox"><img
                        src="{{ asset('/img/questions\/')}}{{ $question->picture }}" class="thumbnail center-block"></a>
            <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox" class="btn btn-info">View Larger
                Image</a>
        </div>
        <br/>
        <div>
            {!! Form::file('picture',null, ['class' => 'form-control']) !!}
            <p>*If you choose another image, it will replace the previous image.</p>
        </div>
    @else
        {!! Form::file('picture', null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}
</div>

<script>
    <?php
    $javascriptOptions = json_encode($options);
    echo "var optionsArray = ". $javascriptOptions . ";\n";
    ?>

    $("#category").autocomplete({
        source: optionsArray
    });

    jQuery.ui.autocomplete.prototype._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
    }

    /*if ($('#category').val().toUpperCase() == 'IMAGE') {
        $('.image_div').css("visibility", "visible");
    } else {
        $('.image_div').css("visibility", "hidden");
    }*/
</script>
