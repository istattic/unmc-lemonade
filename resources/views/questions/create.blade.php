@extends('app')

@section('content')
    <h1>New Question</h1>
    <hr>
    <div class="col-md-6 col-md-offset-3">
        @include('errors.list')
    {!! Form::model($question = new \App\Question, ['url' => 'questions', 'files' => true]) !!}

    @include('questions.form', ['submitButtonText' => 'Add Question'])

    {!! Form::close() !!}


    <br>
    <br>

    </div>
@stop