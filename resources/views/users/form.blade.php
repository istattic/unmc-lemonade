<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('username', 'User Name:') !!}
    {!! Form::input('username', 'username', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::input('email', 'email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('role', 'Role:') !!}
    {!! Form::select('role', array('s' => 'Admin', 'a' => 'Question Contributor'), null, ['class' => 'form-control']) !!}
</div>

<div class="admin">
    <p>*An Admin user is allowed to manage other users accounts, their own account, and questions.</p>
</div>
<div class="user">
    <p>*A Question Contributor is allowed to manage their own account and questions.</p>
</div>

@if (isset($create))
    <div class="form-group">
       {!! Form::label('password', 'Password:') !!}
       {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
       {!! Form::label('password_confirmation', 'Confirm Password:') !!}
       {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
    </div>
@endif

<div class="form-group btn-position-left">
   {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}

</div>
<div>
    <a href="{{ URL::previous() }}" class="btn btn-default btn-position-right form-group">Cancel</a>
</div>

<script>
    jQuery(document).ready(function(){
        $('#role').on('change', function() {
            if ( $("#role").val() == "s")
            {
                $('.admin').show();
                $('.user').hide();
            }
            else
            {
                $('.admin').hide();
                $('.user').show();
            }
        });
    });
</script>