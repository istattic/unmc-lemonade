@extends('app')

@section('content')
    <h1>Edit User:  {{ $user->username }}</h1>
    <hr>
<div class="col-md-6 col-md-offset-3">
    @include('errors.list')
    {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user->id]]) !!}

    @include('users.form', ['submitButtonText' => 'Update User'])

    {!! Form::close() !!}
</div>

    <br>
    <br>


@stop


