@extends('app')

@section('content')
    <h1>New Admin User</h1>
    <hr>

    <div class="col-md-6 col-md-offset-3">
        @include('errors.list')
    {!! Form::model($user = new \App\User,  ['action' => 'UsersController@store']) !!}

    @include('users.form', ['submitButtonText' => 'Add User'])

    {!! Form::close() !!}
    </div>
    <br>
    <br>


@stop

