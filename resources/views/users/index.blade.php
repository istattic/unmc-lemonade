
@extends('app')
<?php $result = count($users);?>
@section('content')

    <h1>Users</h1>
    <hr>

    <table class="table table-hover table-striped table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr class="btnDelete" data-id="{{$user->id}}" da>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <?php $userRole = $user->role ?>
                    @if($userRole == 's')
                        Admin
                    @else
                        Question Contributor
                    @endif
                </td>
                <td>
                    <a class="btn btn-default" href="{{ action('UsersController@edit', [$user->id]) }}">Edit Information</a>
                    <a class="btn btn-default" href="{{ action('UsersController@changePassword', [$user->id]) }}">Change Password</a>
                    <!-- Trigger the modal with a button -->
                    @if($result != 1)
                        @if($user->id !== $loggedInUserId)
                        <button class="btnDelete btn btn-default" data-name="{{$user->username}}" href="">Delete</button>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- start: Delete Coupon Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">Warning!</h3>

                </div>
                <div class="modal-body">

                </div>
                <!--/modal-body-collapse -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnDelteYes" href="#">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
                <!--/modal-footer-collapse -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <div style="clear:both;"><br/></div>
    <div>
        <a class="btn btn-default" href="{{ action('UsersController@create') }}">Add a New
            User</a>
    </div>


    <script>
        $('button.btnDelete').on('click', function (e) {
            e.preventDefault();
            var id = $(this).closest('tr').data('id');
            var name = $(this).attr('data-name');
            $('#myModal').data('id', id, 'name', name).modal('show');
            $('.modal-body').html("<h4>Are you sure you want to delete: " + name + "?</h4>");
        });

        $('#btnDelteYes').click(function () {
            var id = $('#myModal').data('id');
            $.ajax({
                type:"POST",
                url:"/users/"+id+"/destroy",
                data:{
                    _token: "<?php echo csrf_token(); ?>"
                },
                success:function(data){
                    $('[data-id=' + data.id + ']').remove();
                    $('#myModal').modal('hide');
                }
            });

        });

    </script>
    
@stop