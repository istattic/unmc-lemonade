@extends('app')

@section('content')
    <h1>Edit My Information</h1>
    <hr>
    <div class="col-md-6 col-md-offset-3">
        @include('errors.list')
    {!! Form::model($user, ['method' => 'POST', 'action' => ['UsersController@updateAccount', $user->id]]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        {!! Form::label('username', 'User Name:') !!}
        {!! Form::input('username', 'username', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::input('email', 'email', null, ['class' => 'form-control']) !!}
    </div>


    <div class="form-group btn-position-left">
        {!! Form::submit('Update Information',['class' => 'btn btn-default form-control']) !!}
    </div>

    {!! Form::close() !!}


    <div>
        <a href="{{ URL::previous() }}" class="btn btn-default btn-position-right form-group">Cancel</a>
    </div>
    </div>

    <br>


@stop