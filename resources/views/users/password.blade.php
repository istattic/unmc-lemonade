@extends('app')

@section('content')
    <h1>Change Password: {{ $user->username }} </h1>
    <hr>
<div class="col-md-6 col-md-offset-3">
    @include('errors.list')
    {!! Form::model($user, ['method' => 'POST', 'action' => ['UsersController@updatePassword', $user->id]]) !!}
    {!! Form::hidden('hiddenReferField', 'password')!!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        {!! Form::label('password', 'New Password:') !!}
        {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirm Password:') !!}
        {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group btn-position-left">
        {!! Form::submit('Update Password',['class' => 'btn btn-default form-control']) !!}

    </div>

    <div>
        <a href="{{ URL::previous() }}" class="btn btn-default btn-position-right form-group">Cancel</a>
    </div>

    {!! Form::close() !!}
</div>

    <br>
    <br>


@stop
