@extends('app')

@section('content')

    <h1>My Information</h1>
    <hr>

    <table class="table table-hover table-striped table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        </thead>

        <tbody>
            <tr>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a class="btn btn-default" href="{{ action('UsersController@editAccount', [$user->id]) }}">Edit Information</a>
                    <a class="btn btn-default" href="{{ action('UsersController@changeAccountPassword', [$user->id]) }}">Change Password</a>
                </td>
            </tr>
        </tbody>
    </table>

 @stop
