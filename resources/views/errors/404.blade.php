@extends('app')

@section('content')

    <div class="col-md-12">
        <img class="center-block" src="/img/errors/pagenotfounderror.png"/>
        <a href="{{ URL::previous() }}" class="btn btn-primary text-right center-block">Go Back</a>
    </div>


@endsection