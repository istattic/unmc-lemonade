@extends('app')

@section('content')

    {!! Form::open() !!}
    <div class="row">
        <div class="col-md-12 character_header">
            <h1>Which shift would you like to play?</h1>
        </div>
    </div>
    <div class="row selection_row">

        <?php $totalAvailableQuestions = 0; ?>

        @foreach($optionsWithCount as $option)

            <div class="col-sm-10 col-md-3 shift-choice-box">
                <div class="col-sm-9 col-md-12 text-center shift-choice">
                    @if($option['questionCount'] != '0')
                        {!! Form::label($option["category"]) !!}
                    @else
                        {!! Form::label($option["category"],null, array('class' =>'disabledCategoryLabel') ) !!}
                    @endif
                </div>

                <div class="col-sm-3 col-md-12 answer_radio">
                    @if($option['questionCount'] != '0')
                        {!! Form::radio('category', $option['category'], false, ['id' => $option['category'],
                        'required']) !!}
                    @else
                        {!! Form::radio('category', null, false, array('class' => 'category2')) !!}
                    @endif

                </div>

            </div>

            <?php $totalAvailableQuestions = $totalAvailableQuestions + $option['questionCount']; ?>
        @endforeach

        <div class="col-sm-10 col-md-3 text-center shift-choice-box">
                <div class="col-sm-9 col-md-12 text-center shift-choice">
                    @if($totalAvailableQuestions != 0)
                        <label for="random">Grab Bag</label>
                    @else
                        <label for="random" class="disabledCategoryLabel">Grab Bag</label>
                    @endif
                </div>

            <div class="col-sm-3 col-md-12 answer_radio">
                @if($totalAvailableQuestions != 0)
                    {!! Form::radio('category','random', false, ['id' => 'grab_bag','required']) !!}
                @else
                    {!! Form::radio('category', null, false, ['class' => 'category2']) !!}
                @endif
            </div>
        </div>

        <div class="clearfix"></div>


        @if($totalAvailableQuestions != 0)
            <div class="col-md-2 col-md-offset-10">
                <button class="btn btn-primary text-right">Start Shift <i
                            class="glyphicon glyphicon-triangle-right"></i></button>
            </div>
        @else
            <div class="col-md-2 col-md-offset-10">
                <p>There are no questions for this discipline. Want to try another?</p>
                <a href="{{ url('/game/endGame') }}" class="btn btn-primary text-right">Pick New Character <i
                            class="glyphicon glyphicon-triangle-right"></i></a>
            </div>
        @endif


    </div>
    {!! Form::close() !!}

    <script>
        $(document).ready(function () {

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            // By Default Disable radio button
            $(".category2").attr('disabled', true);

        });

    </script>
@endsection