@extends('app')

@section('content')
    <!-- Main component for a primary marketing message or call to action -->
    {!! Form::open() !!}
    <div class="row question_header">
        <div class="col-sm-8 col-md-9">
            <h1>Question</h1>
        </div>
        <div class="col-sm-4 col-md-3">
            @include('partials.player_badge')
        </div>
    </div>
    <div class="row question_text_area">
        <div class="col-md-5 text-center">
            <div class="question_patient_icon">
                <img src="{{asset('/img/characters/patients\/')}}{{ $patientImgURL }}">
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="category_header">
                    <div class="col-md-10">
                        <span class="category_label">CATEGORY:</span>
                        <span class="category"> {{ $question->category }}</span>
                    </div>
                </div>
            </div>
            <div class="question-text">
                {{ $question->question }}
                @if ($question->picture != '')
                    <div class="question_image text-center">
                        <a href="{{ url('/img/questions')}}/{{ $question->picture }}" rel="lightbox"><img
                                    src="{{ asset('/img/questions')}}/{{ $question->picture }}" class="thumbnail center-block"></a>
                        <a href="{{ url('/img/questions')}}/{{ $question->picture }}" rel="lightbox" class="btn btn-info">View Larger
                            Image</a>
                    </div>
                @endif
            </div>
            <br/>
            <div class="category_header">
                <span class="category_label">POSSIBLE SCORE:</span><span
                        class="category value"> ${{ $question->value }}</span>
            </div>
        </div>
    </div>

    <div class="row selection_row">
        <div class="col-sm-6 col-md-3">
            <label for="answer_a">
                <div class="answer_choice center-block">
                    <div class="col-sm-3 col-md-3 pull-left answer_letter">A.</div>
                    <div class="col-sm-9 col-md-9 pull-right answer_text"> {{ $question->answer_a }}</div>
                </div>
            </label>
            <div class="answer_radio">
                {!! Form::radio('answer','a',false,['id' => 'answer_a','required']) !!}
            </div>

        </div>

        <div class="col-sm-6 col-md-3">
            <label for="answer_b">
            <div class="answer_choice center-block">
                <div class="col-sm-3 col-md-3 pull-left answer_letter">B.</div>
                <div class="col-sm-9 col-md-9 pull-right answer_text">{{ $question->answer_b }}</div>
            </div>
                </label>
            <div class="answer_radio">
                {!! Form::radio('answer','b',false, ['id' => 'answer_b']) !!}
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <label for="answer_c">
            <div class="answer_choice center-block">
                <div class="col-sm-3 col-md-3 pull-left answer_letter">C.</div>
                <div class="col-sm-9 col-md-9 pull-right answer_text">{{ $question->answer_c }}</div>
            </div>
            </label>
            <div class="answer_radio">
                {!! Form::radio('answer','c',false, ['id' => 'answer_c']) !!}
            </div>

        </div>

        <div class="col-sm-6 col-md-3">
            <label for="answer_d">
                <div class="answer_choice center-block">
                    <div class="col-sm-3 col-md-3 pull-left answer_letter">D.</div>
                    <div class="col-sm-9 col-md-9 pull-right answer_text">{{ $question->answer_d }}</div>
                </div>
            </label>
            <div class="answer_radio">
                {!! Form::radio('answer','d',false, ['id' => 'answer_d']) !!}
            </div>

        </div>

    </div>

    <div class="row selection_row">
        <div class="col-md-2 col-md-offset-10">
            {!! Form::hidden('question_id',$question->id) !!}

            <button class="btn btn-primary text-right">Continue <i class="glyphicon glyphicon-triangle-right"></i>
            </button>
        </div>
    </div>

    {!! Form::close() !!}

    <script>
        $(document).ready(function () {

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        });
    </script>
@endsection