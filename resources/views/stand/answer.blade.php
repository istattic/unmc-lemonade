@extends('app')

@section('content')
    <!-- Main component for a primary marketing message or call to action -->
    {!! Form::open( ['url' => 'stand', 'method'=>'get' ] ) !!}
    <div class="row question_header">
        <div class="col-sm-8 col-md-9">
            <h1>Answer</h1>
        </div>
        <div class="col-sm-4 col-md-3">
            @include('partials.player_badge')
        </div>
    </div>
    <div class="row question_text_area">
        <div class="col-md-6 text-left">
            <div class="category_header">
                <span class="category_label">CATEGORY:</span> <span class="category"> {{ $question->category }}</span>
            </div>
            <div class="category_header">
                <span class="category_label">POSSIBLE SCORE:</span><span
                        class="category value"> ${{ $question->value }}</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="category_header">
                    <div class="col-md-10"><span class="category_label">LEARNING POINT:</span></div>
                </div>
            </div>
            <div class="question-text">
                {{ $question->learning_point }}
            </div>

        </div>
    </div>

    <div class="row selection_row">
        <div class="col-md-12">
            <div class="answer_explanation col-md-6">
                <div class="col-md-12">
                    @if (strtolower($question->correct_answer) == strtolower($answered))
                        <h2 class="correct-answer">You are correct!</h2>
                    @else
                        <h2 class="incorrect-answer">You were incorrect.</h2>
                        <p>You answered: <strong>{{strtoupper($answered)}}</strong></p>
                    @endif
                </div>
                <div class="col-md-12">
                    <div>
                        <h2>Correct Answer: <span class="category"> {{ $question->correct_answer }}</span></h2>
                    </div>
                    <div>
                        {{ $question->{'answer_' . strtolower($question->correct_answer)} }}
                    </div>
                </div>
                <div class="col-md-12">
                    @if (strtolower($question->correct_answer) == strtolower($answered))
                        <h2>You earned: ${{ $question->value }}</h2>
                    @else
                        <h2>You earned: $0</h2>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <h2>Explanation:</h2>
                    {{ $question->explanation }}
                    @if ($question->picture != '')
                        <div class="question_image text-center">
                            <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox"><img
                                        src="{{ asset('/img/questions\/')}}{{ $question->picture }}" class="thumbnail center-block"></a>
                            <a href="{{ url('/img/questions\/')}}{{ $question->picture }}" rel="lightbox" class="btn btn-info">View Larger
                                Image</a>
                        </div>
                    @endif
                </div>

            </div>
        </div>

    </div>
    <div class="row selection_row">
        <div class="col-md-2 col-md-offset-10">
            <button href="{{ url('/stand/') }}" class="btn btn-primary text-right">Continue <i
                        class="glyphicon glyphicon-triangle-right"></i></button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection