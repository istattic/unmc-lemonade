@extends('app')

@section('content')
    <div class="row question_header">
        <div class="col-md-9 ">
            <h1>Previous Patient Review</h1>

        </div>
        <div class="col-sm-3 ">
            @include('partials.player_badge')
        </div>
        <div class="clearfix"></div>
        <br/>
    </div>

    <div class="row review-patients-row">
        <div class="col-md-6 text-left">
            <div class="category_header">
                 <span class="question_patient_icon">
                    @if (strtolower(json_decode($currentShift['question'])->correct_answer) == strtolower($currentShift['answered']))
                         <img src="{{asset('/img/characters/patients\/')}}{{ $currentShift["patient"] }}-head.png" class="center-block">
                     @else
                         <img src="{{asset('/img/characters/patients\/')}}{{ $currentShift["patient"] }}-alt-head.png"
                              class="center-block">
                     @endif
                </span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="category_header">
                    <div class="col-md-10">
                        <span class="category_label">
                            Earned:
                        </span>
                         <span class="value">
                                @if (strtolower(json_decode($currentShift['question'])->correct_answer) == strtolower($currentShift['answered']))
                                 ${{ json_decode($currentShift['question'])->value }}
                             @else
                                 $0
                             @endif
                        </span>

                    </div>
                    <div class="col-md-10">
                        <div class="category_header">
                            <b>Patient Question:</b> {{ json_decode($currentShift['question'])->question}}
                        </div>
                        <br/>

                        <div>
                            <b>Correct Answer:</b> {{ json_decode($currentShift['question'])->correct_answer}}
                            ({{ json_decode($currentShift['question'])->{'answer_' . strtolower(json_decode($currentShift['question'])->correct_answer)} }}
                            )
                            <br/>
                            <b>You answered:</b> {{ strtoupper($currentShift['answered']) }}
                            ({{json_decode($currentShift['question'])->{'answer_' . strtolower($currentShift['answered'])} }})

                            <br/>
                            <br/>
                            <b>Learning points:</b> {{json_decode($currentShift['question'])->learning_point}}
                        </div>
                    </div>
                    <div class="col-md-2 ">

                    </div>
                </div>
            </div>

        </div>
        <br/>

    </div>
    <div class="row review-patients-row">
        <hr/>
    </div>
    <div class="row selection_row">
        <div class="col-md-2">
        </div>
        <div class="col-md-2 col-md-offset-8">
            <a href="{{ URL::previous() }}" class="btn btn-primary  btn-position-right ">Go Back</a>
        </div>
    </div>
@endsection