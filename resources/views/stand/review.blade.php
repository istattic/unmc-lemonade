@extends('app')

@section('content')
    <div class="row question_header">
        <div class="col-sm-8 col-md-9">
            <h1>Shift Review & Billing</h1>

            <h3>You earned ${{ $allQuestions['earned'] }} out of ${{ $allQuestions['potential'] }}</h3>

            @if($allQuestions['total'] > 0)
                <h3>You answered {{ $allQuestions['correct'] }} out of {{ $allQuestions['total'] }} total questions correctly. ({{ floor(($allQuestions['correct']/$allQuestions['total'])*100) }}% Correct)</h3>
            @else
                <h3>(0% Correct)</h3>
            @endif
        </div>
        <div class="col-sm-4 col-md-3">
            @include('partials.player_badge')
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-sm-2 col-md-2">
            <a href="{{ url('/game/finalReview') }}" class="btn btn-primary text-right">Go to Final Review</a>
        </div>
        <div class="col-sm-offset-9 col-md-2 col-md-offset-8 new_shift">
            <a href="{{ url('/stand/selectShift') }}" class="btn btn-primary text-right pull-right">Start New Shift <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>

    </div>

    <?php $i = 1; ?>
    @foreach($allQuestions['questions'] as $question)
        <div class="row review-patients-row">
            <div class="col-md-6 text-left">
                <div class="category_header">
                <span class="question_patient_icon">
                    @if (strtolower($question['question']->correct_answer) == strtolower($question['answered']))
                        <img src="{{asset('/img/characters/patients\/')}}{{ $question["patient"] }}-head.png" class="center-block">
                    @else
                        <img src="{{asset('/img/characters/patients\/')}}{{ $question["patient"] }}-alt-head.png"
                             class="center-block">
                    @endif
                </span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="category_header">
                        <div class="col-md-10">
                            <div>
                                <b>Category:</b> {{ $question['question']->category }}
                            </div>
                            <br/>
                            <div>
                                <b>Possible Score:</b> ${{ $question['question']->value }}
                                <br/>
                                <span class="category_label">
                                    Earned:
                                </span>
                                <span class="value">
                                        @if (strtolower($question['question']->correct_answer) == strtolower($question['answered']))
                                        ${{ $question['question']->value }}
                                    @else
                                        $0
                                    @endif
                                </span>
                            </div>
                            <br/>
                        </div>

                        <div class="col-md-10">
                            <div class="category_header">
                                <b>Patient Question:</b> {{ $question['question']->question }}
                            </div>
                            <br/>

                            <div>
                                <b>Correct Answer:</b> {{ $question['question']->correct_answer }}
                                ({{ $question['question']->{'answer_' . strtolower($question['question']->correct_answer)} }}
                                )
                                <br/>
                                <b>You Answered:</b> {{ strtoupper($question['answered']) }}
                                ({{ $question['question']->{'answer_' . strtolower($question['answered'])} }})
                            </div>
                            <br/>
                            <div>
                                <b>Learning Point:</b> {{ $question['question']->learning_point }}
                            </div>
                        </div>
                        <div class="col-md-2 ">

                        </div>
                    </div>
                </div>

            </div>
            <br/>

        </div>
        <div class="row review-patients-row">
            <hr/>
        </div>
        <?php $i++; ?>
    @endforeach
    <div class="row selection_row">
        <div class="col-sm-2 col-md-2">
            <a href="{{ url('/game/finalReview') }}" class="btn btn-primary text-right">Go to Final Review</a>
        </div>
        <div class="col-sm-offset-9 col-md-2 col-md-offset-8 new_shift">
            <a href="{{ url('/stand/selectShift') }}" class="btn btn-primary text-right">Start New Shift <i
                        class="glyphicon glyphicon-triangle-right"></i></a>
        </div>
    </div>
@endsection