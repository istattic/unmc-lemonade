@extends('app')

@section('content')
    <!-- Main component for a primary marketing message or call to action -->
    <div class="row sky-setting" style="background-image: url( {{ asset('/img/scenes/settingV02.png')}} );">

        <div class="status_bar">
            <div class="col-sm-3 status_bar_badge">
                @include('partials.player_badge')
            </div>
            <div class="col-sm-3 col-sm-offset-6 status_bar_score">
                @include('partials.player_score')
            </div>
        </div>

        <div class="stand">
            <img class="stand-image" src="{{asset('/img/scenes/stand_v04.png')}}">

            <div class="doctor">
                <img src="{{asset ($doctor->getImgPath()) }}">
            </div>
            <div class="nextPatient">
                {{--<a href="/stand/question" class="btn btn-primary text-right">Next Patient <i class="glyphicon glyphicon-triangle-right"></i></a>--}}
                @if($patientsSeen == 0)
                    <a class="patient-button" href="{{ url('/stand/question') }}">
                        <div class="speech-bubble rounded center-block"><p>Click me to start!</p></div>
                        <img class="patient-image" src="{{asset('/img/characters/patients\/')}}{{ $patientNo }}.png"/></a>
                @else
                    <a class="patient-button" href="{{ url('/stand/question') }}"><img class="patient-image"
                                src="{{asset('/img/characters/patients\/')}}{{ $patientNo }}.png"/></a>
                @endif
            </div>

            @if($patientsSeen != 0)
                <div class="previousPatient">
                    {{--<a href="/stand/previousReview" class="btn btn-primary text-left"> Previous Patient Summary</a>--}}

                    @if (strtolower(json_decode($lastQuestion['question'])->correct_answer) == strtolower($lastQuestion['answered']))
                        <a href="{{ url('/stand/previousReview') }}"> <img class="patient-image" src="{{asset('/img/characters/patients\/')}}{{ $lastCharacter }}.png"
                                                              class="center-block"></a>
                    @else
                        <a href="{{ url('/stand/previousReview') }}"> <img class="patient-image"
                                    src="{{asset('/img/characters/patients\/')}}{{ $lastCharacter }}-alt.png"
                                    class="center-block"></a>
                    @endif
                    <div class="earned_label">
                        <span>
                            Earned:
                        </span>
                        <span>
                            @if (strtolower(json_decode($lastQuestion['question'])->correct_answer) == strtolower($lastQuestion['answered']))
                                ${{ json_decode($lastQuestion['question'])->value }}
                            @else
                                $0
                            @endif
                        </span>
                    </div>
                </div>
            @endif
            <div class="endGame">
                <a href="{{ url('/game/finalReview') }}" class="btn btn-primary text-right">End Game</a>
            </div>
        </div>
    </div>
@endsection