<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">





	<title>UNMC Rheumatology</title>
    <link rel="shortcut icon" href="{{ asset('favicon_lemon_16px.png') }}">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="{{ asset('/css/lightbox.css') }}" rel="stylesheet">

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	{{--<link href="{{ asset('/css/style.css') }}" rel="stylesheet">--}}

    <link href="{{ asset('/css/square/blue.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/square/grey.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{ asset('/js/icheck.js') }}"></script>

    <script src="{{asset('js/jssor.js')}}"></script>
    <script src="{{asset('js/jssor.slider.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
    <script src="{{asset('/js/ckeditor/ckeditor.js')}}"></script>



</head>
<body>
    <div class="container">
        @include('partials.nav')

        @include('flash::message')

        <div class="container-fluid game_screen">
	    @yield('content')
	    </div>
    </div>
	<!-- Scripts -->
    <script src="{{asset('/js/js-webshim/minified/polyfiller.js')}}"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('/js/lightbox.min.js')}}"></script>
	<script type="text/javascript">
	 $('div.alert').not('.alert-important').delay(3000).slideUp()
	</script>
    <script>
        webshim.polyfill('forms');
    </script>

</body>
</html>
