@extends('app')

@section('content')
    <h1>Site Text</h1>
    <hr>

    <table class="table table-hover table-striped table-bordered">
        <thead>
        <tr>
            <th>Section Title</th>
            <th>Section Text</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach($sections as $section)
            <tr>
                <td>{{$section->section_title}}</td>
                <td>{!! $section->section_text !!}</td>
                <td>
                    <a class="btn btn-default" href="{{ action('TextsController@edit', [$section->id]) }}">Edit Text</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop