@extends('app')

@section('content')
    <h1>Edit Section: "{{ $section->section_title }}"</h1>
    <div class="col-md-6 col-md-offset-3">
        @include('errors.list')
    {!! Form::model($section, ['method' => 'PATCH', 'action' => ['TextsController@update', $section->id]]) !!}

    @include('texts.form', ['submitButtonText' => 'Update Section'])

    {!! Form::close() !!}
    </div>

    <br>
    <br>


@stop