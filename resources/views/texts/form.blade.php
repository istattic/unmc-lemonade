<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('section_title', 'Section Title:') !!}
    {!! Form::input('section_title', 'section_title', null, ['class' => 'form-control', 'disabled' => true]) !!}
</div>

<div class="form-group">
    {!! Form::label('section_text', 'Section Text:') !!}
    {!! Form::textarea('section_text', null, ['id' => 'section_text', 'class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn btn-default']) !!}
</div>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'section_text' );
</script>