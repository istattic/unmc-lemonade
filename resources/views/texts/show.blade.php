@extends('app')

@section('content')
    <div>
        <section>
            <h2>
                {{ $section->section_title }}
            </h2>
            <hr/>
            <p>
                {{ $section->section_text }}
            </p>
        </section>
    </div>
    <br/>
    <div class="col-md-12">

        <a class="btn btn-default" href="{{ action('TextsController@index') }}">Return to Sections List</a>

        <a class="btn btn-default" href="{{ action('TextsController@edit', [$section->id]) }}">Edit Section</a>
    </div>


@stop